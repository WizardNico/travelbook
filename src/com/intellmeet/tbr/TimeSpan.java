package com.intellmeet.tbr;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class TimeSpan {
	private Date date;

	public TimeSpan() {
		date = new Date();
	}

	public TimeSpan(Date date) {
		this.date = date;
	}

	public TimeSpan(int hours, int minutes) {
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.HOUR_OF_DAY, hours);
		calendar.set(Calendar.MINUTE, minutes);

		date = calendar.getTime();
	}

	public TimeSpan(String strTime) throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat("HH:mm", Locale.ITALIAN);

		date = sdf.parse(strTime);
	}

	public String toString() {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);

		String strHours = "" + calendar.get(Calendar.HOUR_OF_DAY);
		String strMinutes = "" + calendar.get(Calendar.MINUTE);
		if (strHours.length() == 1)
			strHours = "0" + strHours;

		if (strMinutes.length() == 1)
			strMinutes = "0" + strMinutes;

		String result = (strHours + ":" + strMinutes);

		return (result);
	}

	public int getHours() {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);

		int hours = calendar.get(Calendar.HOUR_OF_DAY);

		return (hours);
	}

	public int getMinutes() {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);

		int minutes = calendar.get(Calendar.MINUTE);

		return (minutes);
	}
}