package com.intellmeet.tbr;

import java.util.ArrayList;
import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

public abstract class MasterAdapter<T> extends ArrayAdapter<T> {
	private ArrayList<T> dataItems;
	private int resItemIndex;

	protected abstract void binding(T dataItem, View layoutItem);

	public MasterAdapter(ArrayList<T> dataItems, int resItemIndex, MasterActivity activity) {
		super(activity, resItemIndex, dataItems);

		this.dataItems = dataItems;
		this.resItemIndex = resItemIndex;
	}

	@SuppressLint("ViewHolder")
	public View getView(int position, View view, ViewGroup group) {
		LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View layoutItem = inflater.inflate(resItemIndex, null);
		T dataItem = dataItems.get(position);

		binding(dataItem, layoutItem);

		return (layoutItem);
	}
}