package com.intellmeet.tbr.services;

import java.io.FileInputStream;
import java.io.ObjectInputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

import org.json.JSONException;
import org.json.JSONObject;

import com.intellmeet.tbr.ParallelTask;
import com.intellmeet.tbr.R;
import com.intellmeet.tbr.activities.MainActivity;
import com.intellmeet.tbr.http.ApiContext;

import android.app.ActivityManager;
import android.app.AlertDialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ActivityManager.RunningServiceInfo;
import android.content.Intent;
import android.widget.Toast;
import android.content.BroadcastReceiver;
import android.content.Context;

public class AlarmReceiver extends BroadcastReceiver {
	private Context context;

	@Override
	public void onReceive(Context context, Intent intent) {
		this.context = context;
		(new CheckSession()).execute();
	}

	private class CheckSession extends ParallelTask {
		protected void beforeExecution() {
		}

		protected Object execution() {
			try {
				// controllo lo stato della sessione
				HashMap<String, Object> cmd = new HashMap<String, Object>();
				JSONObject model = ApiContext.travelBook().get("session/checkSession", cmd);

				return (model);
			} catch (Exception e) {
				return (null);
			}
		}

		protected void progress(Object params) {
		}

		protected void afterExecution(Object output) {
			try {
				// elaboro la risposta del server
				JSONObject model = (JSONObject) output;
				if (model == null) {
					alert(context.getString(R.string.app_errore), context.getString(R.string.app_problema_connessione));
				} else {
					if (model.getBoolean("Data") == true) {
						(new CheckActivateTravel()).execute();
					} else {
						(new Login()).execute();
					}
				}
			} catch (JSONException e) {
				alert(context.getString(R.string.app_errore), context.getString(R.string.app_problema_comunicazione));
			}
		}

		protected void onError() {
		}
	}

	private class Login extends ParallelTask {
		protected void beforeExecution() {
		}

		protected Object execution() {
			try {
				// richiedo l'autenticazione al server
				HashMap<String, Object> cmd = new HashMap<String, Object>();
				cmd.put("Email", readFromDevice("email").toString());
				cmd.put("Password", readFromDevice("password").toString());
				JSONObject model = ApiContext.travelBook().get("session/login", cmd);

				return (model);
			} catch (Exception e) {
				return (null);
			}
		}

		protected void progress(Object params) {
		}

		protected void afterExecution(Object output) {
			try {
				// elaboro la risposta del server
				JSONObject model = (JSONObject) output;
				if (model == null) {
					alert(context.getString(R.string.app_errore), context.getString(R.string.app_problema_connessione));
				} else {
					if (model.getBoolean("Data") == true) {
						(new CheckActivateTravel()).execute();
					} else {
						Toast.makeText(context, context.getString(R.string.access_login_fallito), Toast.LENGTH_LONG).show();
					}
				}
			} catch (JSONException e) {
				alert(context.getString(R.string.app_errore), context.getString(R.string.app_problema_comunicazione));
			}
		}

		protected void onError() {
		}
	}

	private Object readFromDevice(String label) {
		try {
			FileInputStream fis = context.openFileInput(label + ".dat");
			ObjectInputStream ois = new ObjectInputStream(fis);
			Object data = ois.readObject();
			ois.close();

			return (data);
		} catch (Exception e) {
			return (null);
		}
	}

	private class CheckActivateTravel extends ParallelTask {
		protected void beforeExecution() {
		}

		protected Object execution() {
			try {
				// richiedo al server i viaggi
				HashMap<String, Object> cmd = new HashMap<String, Object>();
				JSONObject model = ApiContext.travelBook().get("travels/getActivateTravel", cmd);

				return (model);
			} catch (Exception exception1) {
				return (null);
			}
		}

		protected void progress(Object params) {
		}

		protected void afterExecution(Object output) {
			try {
				JSONObject model = (JSONObject) output;

				// probabile problema di connessione
				if (model == null)
					alert(context.getString(R.string.app_errore), context.getString(R.string.app_problema_connessione));
				else {
					// se c'� un viaggio gi� settato come "in corso"
					if (model.getBoolean("Success") == true) {
						(new CheckOver()).execute();
					} else {
						(new GetNextTravel()).execute();
					}
				}
			} catch (JSONException e) {
				alert(context.getString(R.string.app_errore), context.getString(R.string.app_problema_comunicazione));
			}
		}

		protected void onError() {
		}
	}

	private class CheckOver extends ParallelTask {
		protected void beforeExecution() {
		}

		protected Object execution() {
			try {
				// richiedo al server i viaggi (in maniera paginata)
				HashMap<String, Object> cmd = new HashMap<String, Object>();

				Date date = new Date();
				SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
				String dateTime = sdf.format(date);
				cmd.put("TodayDate", dateTime.toString());

				JSONObject model = ApiContext.travelBook().post("travels/checkTravelOver", cmd);

				return (model);
			} catch (Exception exception1) {
				return (null);
			}
		}

		protected void progress(Object params) {
		}

		protected void afterExecution(Object output) {
			try {
				JSONObject model = (JSONObject) output;

				// probabile problema di connessione
				if (model == null)
					alert(context.getString(R.string.app_errore), context.getString(R.string.app_problema_connessione));
				else {
					// se c'� un viaggio gi� settato come "finito"
					if (model.getBoolean("Success") == true) {
						sendFinishNotification();
						context.stopService(new Intent(context, LocationService.class));
						(new GetNextTravel()).execute();
					} else {
						startLocationService();
					}
				}
			} catch (JSONException e) {
				alert(context.getString(R.string.app_errore), context.getString(R.string.app_problema_comunicazione));
			}
		}

		protected void onError() {
		}
	}

	private class GetNextTravel extends ParallelTask {
		protected void beforeExecution() {
		}

		protected Object execution() {
			try {
				// richiedo al server i viaggi (in maniera paginata)
				HashMap<String, Object> cmd = new HashMap<String, Object>();

				Date date = new Date();
				SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
				String dateTime = sdf.format(date);
				cmd.put("DepartureDate", dateTime.toString());

				JSONObject model = ApiContext.travelBook().get("travels/getNextTravel", cmd);

				return (model);
			} catch (Exception exception1) {
				return (null);
			}
		}

		protected void progress(Object params) {
		}

		protected void afterExecution(Object output) {
			try {
				JSONObject model = (JSONObject) output;

				// probabile problema di connessione
				if (model == null)
					alert(context.getString(R.string.app_errore), context.getString(R.string.app_problema_connessione));
				else {
					// se c'� un viaggio che parte in data odierna
					if (model.getBoolean("Success") == true) {
						JSONObject item = model.getJSONObject("Data");
						sendStartNotification(" " + item.getString("Place") + " ");
						startLocationService();
					}
				}
			} catch (JSONException e) {
				alert(context.getString(R.string.app_errore), context.getString(R.string.app_problema_comunicazione));
			}
		}

		protected void onError() {
		}
	}

	private boolean isMyServiceRunning(Class<?> serviceClass) {
		ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
		for (RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
			if (serviceClass.getName().equals(service.service.getClassName())) {
				return true;
			}
		}
		return false;
	}

	private void startLocationService() {
		if (!isMyServiceRunning(LocationService.class))
			context.startService(new Intent(context, LocationService.class));
	}

	public void sendStartNotification(String placeName) {
		Intent notificationIntent = new Intent(context, MainActivity.class);
		notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
		PendingIntent intent = PendingIntent.getActivity(context, 0, notificationIntent, 0);

		final Notification.Builder builder = new Notification.Builder(context);
		builder.setStyle(
				new Notification.BigTextStyle(builder).bigText(context.getString(R.string.notification_start_note1) + placeName + context.getString(R.string.notification_start_note2))
						.setBigContentTitle(context.getString(R.string.notification_start_message)).setSummaryText(context.getString(R.string.notification_summary)))
				.setContentTitle(context.getString(R.string.notification_start_message))
				.setContentText(context.getString(R.string.notification_start_note1) + placeName + context.getString(R.string.notification_start_note2)).setSmallIcon(R.drawable.ic_notification)
				.setDefaults(Notification.DEFAULT_SOUND | Notification.DEFAULT_VIBRATE | Notification.DEFAULT_LIGHTS).setAutoCancel(true).setContentIntent(intent);

		NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
		notificationManager.notify(0, builder.build());
	}

	public void sendFinishNotification() {
		Intent notificationIntent = new Intent(context, MainActivity.class);
		notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
		PendingIntent intent = PendingIntent.getActivity(context, 0, notificationIntent, 0);

		final Notification.Builder builder = new Notification.Builder(context);
		builder.setStyle(
				new Notification.BigTextStyle(builder).bigText(context.getString(R.string.notification_finish_note)).setBigContentTitle(context.getString(R.string.notification_finish_message))
						.setSummaryText(context.getString(R.string.notification_summary))).setContentTitle(context.getString(R.string.notification_finish_message))
				.setContentText(context.getString(R.string.notification_finish_note)).setSmallIcon(R.drawable.ic_notification)
				.setDefaults(Notification.DEFAULT_SOUND | Notification.DEFAULT_VIBRATE | Notification.DEFAULT_LIGHTS).setAutoCancel(true).setContentIntent(intent);

		NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
		notificationManager.notify(0, builder.build());
	}

	private void alert(String title, String message) {
		AlertDialog.Builder alertBuilder = new AlertDialog.Builder(context, AlertDialog.THEME_HOLO_DARK);
		alertBuilder.setTitle(title);
		alertBuilder.setMessage(message);
		AlertDialog alert = alertBuilder.create();
		alert.show();
	}
}
