package com.intellmeet.tbr.services;

import java.io.FileInputStream;
import java.io.ObjectInputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import org.json.JSONException;
import org.json.JSONObject;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.intellmeet.tbr.ParallelTask;
import com.intellmeet.tbr.R;
import com.intellmeet.tbr.http.ApiContext;

import android.app.AlertDialog;
import android.app.Service;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.os.IBinder;
import android.widget.Toast;

public class LocationService extends Service implements ConnectionCallbacks, OnConnectionFailedListener, LocationListener {

	private Location mLastLocation;
	private int travelID;

	// Google client to interact with Google API
	private GoogleApiClient mGoogleApiClient;

	private LocationRequest mLocationRequest;

	// Location updates intervals in sec
	private static int UPDATE_INTERVAL = 10000; // 10 sec
	private static int FATEST_INTERVAL = 5000; // 5 sec
	private static int DISPLACEMENT = 3000; // 3km

	@Override
	public void onCreate() {
		super.onCreate();

		// First we need to check availability of play services
		if (checkPlayServices()) {
			// Building the GoogleApi client
			buildGoogleApiClient();
			createLocationRequest();
			(new CheckSession()).execute();
		}
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		start();
		return START_STICKY;
	}

	private void start() {
		if (mGoogleApiClient != null) {
			mGoogleApiClient.connect();
		}
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
	}

	public void onResume() {
		checkPlayServices();
		// Resuming the periodic location updates
		if (mGoogleApiClient.isConnected()) {
			startLocationUpdates();
		}
	}

	public void onStop() {
		if (mGoogleApiClient.isConnected()) {
			LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
			mGoogleApiClient.disconnect();
		}
	}

	public void onPause() {
		stopLocationUpdates();
	}

	/**
	 * Creating google api client object
	 */
	protected synchronized void buildGoogleApiClient() {
		mGoogleApiClient = new GoogleApiClient.Builder(this).addApi(LocationServices.API).addConnectionCallbacks(this).addOnConnectionFailedListener(this).build();
	}

	/**
	 * Creating location request object
	 */
	protected void createLocationRequest() {
		mLocationRequest = new LocationRequest();
		mLocationRequest.setInterval(UPDATE_INTERVAL);
		mLocationRequest.setFastestInterval(FATEST_INTERVAL);
		mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
		mLocationRequest.setSmallestDisplacement(DISPLACEMENT);
	}

	/**
	 * Method to verify google play services on the device
	 */
	private boolean checkPlayServices() {
		GoogleApiAvailability googleAPI = GoogleApiAvailability.getInstance();
		int result = googleAPI.isGooglePlayServicesAvailable(this);
		if (result != ConnectionResult.SUCCESS) {
			if (googleAPI.isUserResolvableError(result)) {
				Toast.makeText(getApplicationContext(), "Code Error: " + result, Toast.LENGTH_LONG).show();
			} else {
				Toast.makeText(getApplicationContext(), "This device is not supported.", Toast.LENGTH_LONG).show();
				stopSelf();
			}
			return false;
		}
		return true;
	}

	/**
	 * Starting the location updates
	 */
	protected void startLocationUpdates() {
		LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
	}

	/**
	 * Stopping location updates
	 */
	protected void stopLocationUpdates() {
		LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
	}

	/**
	 * Google api callback methods
	 */
	@Override
	public void onConnectionFailed(ConnectionResult result) {
		Toast.makeText(getApplicationContext(), "Connection failed: ConnectionResult.getErrorCode() = " + result.getErrorCode(), Toast.LENGTH_SHORT).show();
	}

	@Override
	public void onConnected(Bundle arg0) {
		// Once connected with google api, get the location
		startLocationUpdates();
	}

	@Override
	public void onConnectionSuspended(int arg0) {
		mGoogleApiClient.connect();
	}

	@Override
	public void onLocationChanged(Location location) {
		// Assign the new location
		mLastLocation = location;

		if (mLastLocation != null) {
			(new AddTag(mLastLocation)).execute();
		} else
			Toast.makeText(getApplicationContext(), "Couldn't get the location. Make sure location is enabled on the device", Toast.LENGTH_SHORT).show();
	}

	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}

	private class CheckSession extends ParallelTask {
		protected void beforeExecution() {
		}

		protected Object execution() {
			try {
				// controllo lo stato della sessione
				HashMap<String, Object> cmd = new HashMap<String, Object>();
				JSONObject model = ApiContext.travelBook().get("session/checkSession", cmd);

				return (model);
			} catch (Exception e) {
				return (null);
			}
		}

		protected void progress(Object params) {
		}

		protected void afterExecution(Object output) {
			try {
				// elaboro la risposta del server
				JSONObject model = (JSONObject) output;
				if (model == null) {
					Toast.makeText(getApplicationContext(), getString(R.string.app_problema_connessione), Toast.LENGTH_SHORT).show();
				} else {
					if (model.getBoolean("Data") == true) {
						(new CheckActivateTravel()).execute();
					} else {
						(new Login()).execute();
					}
				}
			} catch (JSONException e) {
				Toast.makeText(getApplicationContext(), getString(R.string.app_problema_comunicazione), Toast.LENGTH_SHORT).show();
			}
		}

		protected void onError() {
		}
	}

	private class Login extends ParallelTask {
		protected void beforeExecution() {
		}

		protected Object execution() {
			try {
				// richiedo l'autenticazione al server
				HashMap<String, Object> cmd = new HashMap<String, Object>();
				cmd.put("Email", readFromDevice("email").toString());
				cmd.put("Password", readFromDevice("password").toString());
				JSONObject model = ApiContext.travelBook().get("session/login", cmd);

				return (model);
			} catch (Exception e) {
				return (null);
			}
		}

		protected void progress(Object params) {
		}

		protected void afterExecution(Object output) {
			try {
				// elaboro la risposta del server
				JSONObject model = (JSONObject) output;
				if (model == null) {
					Toast.makeText(getApplicationContext(), getString(R.string.app_problema_connessione), Toast.LENGTH_SHORT).show();
				} else {
					if (model.getBoolean("Data") == true) {
						(new CheckActivateTravel()).execute();
					} else {
						Toast.makeText(getApplicationContext(), getString(R.string.access_login_fallito), Toast.LENGTH_LONG).show();
					}
				}
			} catch (JSONException e) {
				alert(getString(R.string.app_errore), getString(R.string.app_problema_comunicazione));
			}
		}

		protected void onError() {
		}
	}

	private class CheckActivateTravel extends ParallelTask {
		protected void beforeExecution() {
		}

		protected Object execution() {
			try {
				// richiedo al server i viaggi
				HashMap<String, Object> cmd = new HashMap<String, Object>();
				JSONObject model = ApiContext.travelBook().get("travels/getActivateTravel", cmd);

				return (model);
			} catch (Exception exception1) {
				return (null);
			}
		}

		protected void progress(Object params) {
		}

		protected void afterExecution(Object output) {
			try {
				JSONObject model = (JSONObject) output;

				// probabile problema di connessione
				if (model != null) {
					// se c'� un viaggio gi� settato come "in corso"
					if (model.getBoolean("Success") == true) {
						JSONObject item = model.getJSONObject("Data");
						travelID = item.getInt("TravelID");
					}
				}
			} catch (JSONException e) {
				Toast.makeText(getApplicationContext(), getString(R.string.app_problema_comunicazione), Toast.LENGTH_SHORT).show();
			}
		}

		protected void onError() {
		}
	}

	private class AddTag extends ParallelTask {
		private Location location;

		public AddTag(Location location) {
			this.location = location;
		}

		protected void beforeExecution() {
		}

		protected Object execution() {
			try {
				// salvo le coordinate sul server
				HashMap<String, Object> cmd = new HashMap<String, Object>();
				cmd.put("Latitude", location.getLatitude());
				cmd.put("Longitude", location.getLongitude());

				Date date = new Date(location.getTime());
				SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm");
				String dateTime = sdf.format(date);

				Geocoder gcd = new Geocoder(LocationService.this, Locale.getDefault());
				List<Address> addresses = gcd.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
				Address address = addresses.get(0);

				if (address != null) {
					cmd.put("Name", address.getLocality());
					if (address.getAdminArea() != null)
						cmd.put("Area", address.getAdminArea());
					else
						cmd.put("Area", address.getSubAdminArea());
					cmd.put("Address", address.getAddressLine(0));
					cmd.put("Date", dateTime.toString());
				} else {
					cmd.put("Name", "Registrazione");
					cmd.put("Area", "");
					cmd.put("Address", "");
					cmd.put("Date", "");
				}

				cmd.put("TravelID", String.valueOf(travelID));

				JSONObject model = ApiContext.travelBook().post("tags/addTag", cmd);

				return (model);
			} catch (Exception e) {
				return (null);
			}
		}

		protected void progress(Object params) {
		}

		protected void afterExecution(Object output) {
			try {
				// elaboro la risposta del server
				JSONObject model = (JSONObject) output;
				if (model != null) {
					if (model.getBoolean("Success") == false)
						Toast.makeText(getApplicationContext(), model.getString("ErrorDescription"), Toast.LENGTH_LONG).show();
				}
			} catch (JSONException e) {
				Toast.makeText(getApplicationContext(), getString(R.string.app_problema_comunicazione), Toast.LENGTH_LONG).show();
			}
		}

		protected void onError() {
		}
	}

	private Object readFromDevice(String label) {
		try {
			FileInputStream fis = getApplicationContext().openFileInput(label + ".dat");
			ObjectInputStream ois = new ObjectInputStream(fis);
			Object data = ois.readObject();
			ois.close();

			return (data);
		} catch (Exception e) {
			return (null);
		}
	}

	private void alert(String title, String message) {
		AlertDialog.Builder alertBuilder = new AlertDialog.Builder(getApplicationContext(), AlertDialog.THEME_HOLO_DARK);
		alertBuilder.setTitle(title);
		alertBuilder.setMessage(message);
		AlertDialog alert = alertBuilder.create();
		alert.show();
	}
}