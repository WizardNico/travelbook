package com.intellmeet.tbr.fragments;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView.OnItemClickListener;

import com.intellmeet.tbr.http.ApiContext;
import com.intellmeet.tbr.MasterFragment;
import com.intellmeet.tbr.Pagination;
import com.intellmeet.tbr.ParallelTask;
import com.intellmeet.tbr.R;
import com.intellmeet.tbr.activities.TagDetailsActivity;
import com.intellmeet.tbr.activities.TagsActivity;
import com.intellmeet.tbr.adapters.TagAdapter;
import com.intellmeet.tbr.adapters.dataitems.TagsAdapterDataItem;

public class TagsFragment extends MasterFragment {
	private final int PAGE_SIZE = 15;
	private final int IMG_MAX_DIM_SIZE = 100;
	private final int REQUEST_IMAGE_CAPTURE_TAG = 3;
	private final int REQUEST_IMAGE_GALLERY_TAG = 4;

	private int pageIndex;
	private int travelId;
	private boolean load;
	private TagAdapter tagsListAdapter;
	private ProgressBar tagsListWaiting;
	private List<Integer> tagsListIds;
	private Pagination pagination;

	private ListView getTagsLswTags() {
		return ((ListView) getLauncherActivity().findViewById(R.id.tagsLswTravels));
	}

	private SwipeRefreshLayout getTagsSwrRefresh() {
		return ((SwipeRefreshLayout) getLauncherActivity().findViewById(R.id.tagsSwrRefresh));
	}

	private ProgressBar getTravelsPrbWaiting() {
		return ((ProgressBar) getLauncherActivity().findViewById(R.id.tagsPrbWaiting));
	}

	public View onCreateView(LayoutInflater inflater, ViewGroup group, Bundle bundle) {
		pageIndex = 1;

		load = true;

		ArrayList<TagsAdapterDataItem> adapterItems = new ArrayList<TagsAdapterDataItem>();
		tagsListAdapter = new TagAdapter(adapterItems, getLauncherActivity());

		tagsListWaiting = new ProgressBar(getLauncherActivity());

		tagsListIds = new ArrayList<Integer>();

		pagination = new Pagination(PAGE_SIZE);

		travelId = (Integer)getLauncherActivity().getActivityParam("Id");

		return (inflater.inflate(R.layout.tags_frame, group, false));
	}

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo) {
		AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuInfo;
		menu.setHeaderTitle(tagsListAdapter.getItem(info.position).getTitle());
		getLauncherActivity().getMenuInflater().inflate(R.menu.popup_menu, menu);
	}

	@Override
	public void onResume() {
		super.onResume();
		pageIndex = 1;
		(new GetTags()).execute();
	}

	public void onViewCreated(View view, Bundle bundle) {
		super.onViewCreated(view, bundle);

		getTagsLswTags().setAdapter(tagsListAdapter);

		// listener per la paginazione
		getTagsLswTags().setOnScrollListener(new OnScrollListener() {
			public void onScrollStateChanged(AbsListView listView, int scrollState) {
			}

			public void onScroll(AbsListView listView, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
				if (pagination.nextPage(firstVisibleItem, visibleItemCount, totalItemCount, load)) {
					pageIndex++;
					
					(new GetTags()).execute();
				}
			}
		});

		// listener per il refresh
		getTagsSwrRefresh().setOnRefreshListener(new OnRefreshListener() {
			public void onRefresh() {
				pageIndex = 1;
				load = true;
				
				(new GetTags()).execute();
			}
		});

		// listener per l'apertura dei dettagli
		getTagsLswTags().setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
				HashMap<String, Object> params = new HashMap<String, Object>();
				params.put("Id", tagsListIds.get(position));
				params.put("TravelId", travelId);
				params.put("State", 0);

				getLauncherActivity().activityOpen(TagDetailsActivity.class, false, params);
			}
		});

		// settaggio grafico delle rotelle di attesa
		getTagsSwrRefresh().setColorSchemeResources(android.R.color.holo_blue_bright, android.R.color.holo_green_light, android.R.color.holo_orange_light, android.R.color.holo_red_light);
		getLauncherActivity().setTop(getTravelsPrbWaiting(), 45);

		// espongo il campo di ricerca
		setHasOptionsMenu(true);

		registerForContextMenu(getTagsLswTags());

		(new GetTags()).execute();
	}

	@Override
	public boolean onContextItemSelected(MenuItem item) {
		AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
		final int pos = info.position;
		int menuItemIndex = item.getItemId();
		switch (menuItemIndex) {
		// Elimina tag
		case R.id.menu_delete:
			getLauncherActivity().confirmAlert("Elimina Tag", "Sei sicuro di voler eliminare questa registrazione?", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {
					(new DeleteTag(tagsListIds.get(pos))).execute();
				}
			}, null);
			break;

		// Apri tag
		case R.id.menu_open:
			HashMap<String, Object> params = new HashMap<String, Object>();
			params.put("Id", tagsListIds.get(pos));
			params.put("TravelId", travelId);
			params.put("State", 0);
			getLauncherActivity().activityOpen(TagDetailsActivity.class, false, params);
			break;

		// Aggiungo la foto
		case R.id.menu_picture:
			List<String> listItems = new ArrayList<String>();
			listItems.add("Fotocamera");
			listItems.add("Galleria");

			final CharSequence[] charSequenceItems = listItems.toArray(new CharSequence[listItems.size()]);

			AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(), 2);
			builder.setTitle("Inserimento Foto").setItems(charSequenceItems, new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int position) {
					switch (position) {
					case 0:
						ContentValues values = new ContentValues();
						values.put(MediaStore.Images.Media.TITLE, "New Picture");
						values.put(MediaStore.Images.Media.DESCRIPTION, "From your Camera");
						Uri imageUri = getLauncherActivity().getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
						Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
						takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
						if (takePictureIntent.resolveActivity(getLauncherActivity().getPackageManager()) != null) {
							((TagsActivity) getActivity()).setId(tagsListIds.get(pos));
							((TagsActivity) getActivity()).saveToDevice("tagId", tagsListIds.get(pos).toString());
							((TagsActivity) getActivity()).saveToDevice("uriNewImage", imageUri.toString());
							getLauncherActivity().startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE_TAG);
						}
						break;

					case 1:
						Intent galleryIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
						if (galleryIntent.resolveActivity(getLauncherActivity().getPackageManager()) != null) {
							((TagsActivity) getActivity()).setId(tagsListIds.get(pos));
							getLauncherActivity().startActivityForResult(galleryIntent, REQUEST_IMAGE_GALLERY_TAG);
						}
						break;
					}
				}
			});

			builder.create();
			builder.show();

			break;
		}
		return true;
	}

	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		super.onCreateOptionsMenu(menu, inflater);

		// imposto il menu
		inflater.inflate(R.menu.tags_menu, menu);
	}

	private class DeleteTag extends ParallelTask {
		private int position;

		public DeleteTag(int position) {
			this.position = position;
		}

		protected void beforeExecution() {
		}

		protected Object execution() {
			try {
				// richiedo al server l'inserimento
				HashMap<String, Object> cmd = new HashMap<String, Object>();
				cmd.put("TagID", this.position);

				JSONObject model = ApiContext.travelBook().delete("tags/removeTag", cmd, getLauncherActivity());

				return (model);
			} catch (Exception e) {
				return (null);
			}
		}

		protected void progress(Object params) {
		}

		protected void afterExecution(Object output) {
			try {
				// elaboro la risposta del server
				JSONObject model = (JSONObject) output;
				if (model == null)
					getLauncherActivity().alert(getString(R.string.app_errore), getString(R.string.app_problema_connessione));
				else {
					if (model.getBoolean("Success") == true) {
						// richiedo al server i viaggi (in maniera paginata)
						HashMap<String, Object> param = new HashMap<String, Object>();
						param.put("Id", travelId);
						getLauncherActivity().activityOpen(TagsActivity.class, true, param);
					} else
						getLauncherActivity().toast(model.getString("ErrorDescription"));
				}
			} catch (JSONException e) {
				getLauncherActivity().alert(getString(R.string.app_errore), getString(R.string.app_problema_comunicazione));
			}
		}

		protected void onError() {
		}
	}

	private class GetTags extends ParallelTask {
		protected void beforeExecution() {
		}

		protected Object execution() {
			try {
				// richiedo al server i tag (in maniera paginata)
				HashMap<String, Object> cmd = new HashMap<String, Object>();
				cmd.put("PageIndex", pageIndex);
				cmd.put("PageSize", PAGE_SIZE);
				cmd.put("TravelID", travelId);
				JSONObject model = ApiContext.travelBook().get("tags/getTags", cmd, getLauncherActivity());

				getLauncherActivity().saveToDevice("tags" + pageIndex + travelId, model.toString());

				// scarico le immagini
				for (int i = 0; i < model.getJSONObject("Data").getJSONArray("List").length(); i++) {
					JSONObject item = model.getJSONObject("Data").getJSONArray("List").getJSONObject(i);
					if (item.getString("ImageUrl").compareTo("null") != 0 && getLauncherActivity().getUrlImage(item.getString("ImageUrl"), IMG_MAX_DIM_SIZE) == null)
						downloadUrlImage(item.getString("ImageUrl"), getLauncherActivity());
				}

				return (model);
			} catch (Exception exception1) {
				try {
					// prelevo il risultato dell'ultima chiamata andata a buon
					// fine
					JSONObject oldModel = null;
					String strOldModel = (String) getLauncherActivity().readFromDevice("tags" + pageIndex + travelId);
					if (strOldModel != null)
						oldModel = new JSONObject(strOldModel);

					return (oldModel);
				} catch (Exception exception2) {
					return (null);
				}
			}
		}

		protected void progress(Object params) {
		}

		protected void afterExecution(Object output) {
			try {
				// nascondo la rotella di attesa
				getTagsSwrRefresh().setRefreshing(false);
				getLauncherActivity().componentHide(getTravelsPrbWaiting());

				JSONObject model = (JSONObject) output;

				// probabile problema di connessione
				if (model == null)
					getLauncherActivity().alert(getString(R.string.app_errore), getString(R.string.app_problema_connessione));
				else {
					if (pageIndex > model.getJSONObject("Data").getLong("PageCount")) {
						load = false;
						return;
					} else {
						// quando si fa il refresh la lista deve essere svuotata
						if (pageIndex == 1) {
							tagsListAdapter.clear();
							tagsListIds.clear();
						}

						// compilo la lista di viaggi
						for (int i = 0; i < model.getJSONObject("Data").getJSONArray("List").length(); i++) {
							JSONObject item = model.getJSONObject("Data").getJSONArray("List").getJSONObject(i);

							Bitmap image = null;
							if (item.getString("ImageUrl").compareTo("null") != 0)
								image = getLauncherActivity().getUrlImage(item.getString("ImageUrl"), IMG_MAX_DIM_SIZE);
							else
								image = getLauncherActivity().getResImage(R.drawable.no_photo_img, IMG_MAX_DIM_SIZE);

							tagsListIds.add(item.getInt("TagID"));
							String note = item.getString("Note");
							if (note.toString().equals("null"))
								tagsListAdapter.add(new TagsAdapterDataItem(item.getString("Name"), getLauncherActivity().dateTimeFormat(getLauncherActivity().jsonDateParse(item.getString("Date"))),
										image, ""));
							else
								tagsListAdapter.add(new TagsAdapterDataItem(item.getString("Name"), getLauncherActivity().dateTimeFormat(getLauncherActivity().jsonDateParse(item.getString("Date"))),
										image, note));
						}

						tagsListAdapter.notifyDataSetChanged();

						// gestione del footer di attesa
						if (getTagsLswTags().getFooterViewsCount() == 0) {
							getTagsLswTags().setAdapter(tagsListAdapter);
							getTagsLswTags().addFooterView(tagsListWaiting);
						}

						if (pageIndex >= model.getJSONObject("Data").getLong("PageCount")) {
							load = false;
							getTagsLswTags().setAdapter(tagsListAdapter);
							getTagsLswTags().removeFooterView(tagsListWaiting);
							getTagsLswTags().setSelection(model.getJSONObject("Data").getJSONArray("List").length());
						}

						// se la lista dei risultati � vuota:
						if (model.getJSONObject("Data").getLong("RecordCount") == 0)
							getLauncherActivity().toast(getString(R.string.tag_nessun_risultato));
					}
				}
			} catch (JSONException e) {
				getLauncherActivity().alert(getString(R.string.app_errore), getString(R.string.app_problema_comunicazione));
			}
		}

		protected void onError() {
		}
	}
}