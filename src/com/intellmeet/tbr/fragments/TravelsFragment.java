package com.intellmeet.tbr.fragments;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.SearchView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView.OnItemClickListener;

import com.intellmeet.tbr.http.ApiContext;
import com.intellmeet.tbr.services.LocationService;
import com.intellmeet.tbr.MasterFragment;
import com.intellmeet.tbr.Pagination;
import com.intellmeet.tbr.ParallelTask;
import com.intellmeet.tbr.R;
import com.intellmeet.tbr.activities.MainActivity;
import com.intellmeet.tbr.activities.TagsActivity;
import com.intellmeet.tbr.adapters.TravelAdapter;
import com.intellmeet.tbr.adapters.dataitems.TravelsAdapterDataItem;

public class TravelsFragment extends MasterFragment {
	private final int PAGE_SIZE = 10;
	private final int IMG_MAX_DIM_SIZE = 100;
	private final int REQUEST_IMAGE_CAPTURE = 1;
	private final int REQUEST_IMAGE_GALLERY = 2;

	private int pageIndex;
	private boolean load;
	private TravelAdapter travelsListAdapter;
	private ProgressBar travelsListWaiting;
	private List<Integer> travelsListIds;
	private Pagination pagination;
	private String genericFilter;

	private ListView getTravelsLswTravels() {
		return ((ListView) getLauncherActivity().findViewById(R.id.travelsLswTravels));
	}

	private SwipeRefreshLayout getTravelsSwrRefresh() {
		return ((SwipeRefreshLayout) getLauncherActivity().findViewById(R.id.travelsSwrRefresh));
	}

	private ProgressBar getTravelsPrbWaiting() {
		return ((ProgressBar) getLauncherActivity().findViewById(R.id.travelsPrbWaiting));
	}

	private ImageButton getTravelsBtnAddTravel() {
		return ((ImageButton) getLauncherActivity().findViewById(R.id.travelsBtnAddTravel));
	}

	public View onCreateView(LayoutInflater inflater, ViewGroup group, Bundle bundle) {
		pageIndex = 1;
		load = true;

		ArrayList<TravelsAdapterDataItem> adapterItems = new ArrayList<TravelsAdapterDataItem>();
		travelsListAdapter = new TravelAdapter(adapterItems, getLauncherActivity());

		travelsListWaiting = new ProgressBar(getLauncherActivity());

		travelsListIds = new ArrayList<Integer>();

		pagination = new Pagination(PAGE_SIZE);

		genericFilter = null;

		return (inflater.inflate(R.layout.travels_frame, group, false));
	}

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo) {
		AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuInfo;
		menu.setHeaderTitle(travelsListAdapter.getItem(info.position).getTitle());
		getLauncherActivity().getMenuInflater().inflate(R.menu.popup_menu, menu);
	}

	public void onViewCreated(View view, Bundle bundle) {
		super.onViewCreated(view, bundle);

		getTravelsLswTravels().setAdapter(travelsListAdapter);

		// listener per la paginazione
		getTravelsLswTravels().setOnScrollListener(new OnScrollListener() {
			public void onScrollStateChanged(AbsListView listView, int scrollState) {
			}

			public void onScroll(AbsListView listView, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
				if (pagination.nextPage(firstVisibleItem, visibleItemCount, totalItemCount, load)) {
					pageIndex++;
					
					(new GetTravels()).execute();
				}
			}
		});

		// listener per il refresh
		getTravelsSwrRefresh().setOnRefreshListener(new OnRefreshListener() {
			public void onRefresh() {
				pageIndex = 1;
				load = true;
				(new GetTravels()).execute();
			}
		});

		// listener per l'apertura dei dettagli
		getTravelsLswTravels().setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
				HashMap<String, Object> params = new HashMap<String, Object>();
				params.put("Id", travelsListIds.get(position));

				getLauncherActivity().activityOpen(TagsActivity.class, false, params);
			}
		});

		// settaggio grafico delle rotelle di attesa
		getTravelsSwrRefresh().setColorSchemeResources(android.R.color.holo_blue_bright, android.R.color.holo_green_light, android.R.color.holo_orange_light, android.R.color.holo_red_light);
		getLauncherActivity().setTop(getTravelsPrbWaiting(), 45);

		// espongo il campo di ricerca
		setHasOptionsMenu(true);

		// posizionamento del pulsante per l'inserimento di un nuovo viaggio
		getLauncherActivity().setBottom(getTravelsBtnAddTravel(), 17);

		registerForContextMenu(getTravelsLswTravels());

		(new GetTravels()).execute();
	}

	@Override
	public boolean onContextItemSelected(MenuItem item) {
		AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
		final int pos = info.position;
		int menuItemIndex = item.getItemId();
		switch (menuItemIndex) {
		// Elimina viaggio
		case R.id.menu_delete:
			getLauncherActivity().confirmAlert("Elimina Viaggio", "Sei sicuro di voler eliminare questo viaggio?", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {
					(new DeleteTravel(pos)).execute();
				}
			}, null);
			break;

		// Apri viaggio
		case R.id.menu_open:
			HashMap<String, Object> params = new HashMap<String, Object>();
			params.put("Id", travelsListIds.get(info.position));
			getLauncherActivity().activityOpen(TagsActivity.class, true, params);
			break;

		// Foto copertina
		case R.id.menu_picture:
			List<String> listItems = new ArrayList<String>();
			listItems.add("Fotocamera");
			listItems.add("Galleria");

			final CharSequence[] charSequenceItems = listItems.toArray(new CharSequence[listItems.size()]);

			AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(), 2);
			builder.setTitle("Inserimento Foto").setItems(charSequenceItems, new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int position) {

					switch (position) {
					case 0:
						ContentValues values = new ContentValues();
						values.put(MediaStore.Images.Media.TITLE, "New Picture");
						values.put(MediaStore.Images.Media.DESCRIPTION, "From your Camera");
						Uri imageUri = getLauncherActivity().getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
						Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
						takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
						if (takePictureIntent.resolveActivity(getLauncherActivity().getPackageManager()) != null) {
							((MainActivity) getActivity()).setId(travelsListIds.get(pos));
							((MainActivity) getActivity()).saveToDevice("travelId", travelsListIds.get(pos).toString());
							((MainActivity) getActivity()).setUri(imageUri);
							((MainActivity) getActivity()).saveToDevice("uriNewImage", imageUri.toString());
							getLauncherActivity().startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
						}
						break;

					case 1:
						Intent galleryIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
						if (galleryIntent.resolveActivity(getLauncherActivity().getPackageManager()) != null) {
							((MainActivity) getActivity()).setId(travelsListIds.get(pos));
							getLauncherActivity().startActivityForResult(galleryIntent, REQUEST_IMAGE_GALLERY);
						}
						break;
					}
				}
			});

			builder.create();
			builder.show();

			break;
		}
		return true;
	}

	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		super.onCreateOptionsMenu(menu, inflater);

		// imposto il menu
		inflater.inflate(R.menu.filter_menu, menu);

		// listener sul menu
		((SearchView) menu.findItem(R.id._filterMenuFilter).getActionView()).setOnQueryTextListener(new SearchView.OnQueryTextListener() {
			public boolean onQueryTextSubmit(String text) {
				travelsListAdapter.clear();
				travelsListIds.clear();
				getLauncherActivity().componentShow(getTravelsPrbWaiting());
				if (getTravelsLswTravels().getFooterViewsCount() == 1) {
					getTravelsLswTravels().setAdapter(travelsListAdapter);
					getTravelsLswTravels().removeFooterView(travelsListWaiting);
				}

				genericFilter = text;
				pageIndex = 1;
				load = true;
				(new GetTravels()).execute();

				return (false);
			}

			public boolean onQueryTextChange(String text) {
				if (text == null || text.length() == 0) {
					travelsListAdapter.clear();
					travelsListIds.clear();
					getLauncherActivity().componentShow(getTravelsPrbWaiting());
					if (getTravelsLswTravels().getFooterViewsCount() == 1) {
						getTravelsLswTravels().setAdapter(travelsListAdapter);
						getTravelsLswTravels().removeFooterView(travelsListWaiting);
					}

					genericFilter = null;
					pageIndex = 1;
					load = true;
					(new GetTravels()).execute();
				}

				return (false);
			}
		});
	}

	private class GetTravels extends ParallelTask {
		protected void beforeExecution() {
		}

		protected Object execution() {
			try {
				// richiedo al server i viaggi (in maniera paginata)
				HashMap<String, Object> cmd = new HashMap<String, Object>();
				cmd.put("PageIndex", pageIndex);
				cmd.put("PageSize", PAGE_SIZE);
				if (genericFilter != null)
					cmd.put("GenericFilter", genericFilter);
				JSONObject model = ApiContext.travelBook().get("travels/getTravels", cmd, getLauncherActivity());

				// salvo il risultato su disco
				if (genericFilter == null || genericFilter.length() == 0)
					getLauncherActivity().saveToDevice("travels" + pageIndex, model.toString());

				// scarico le immagini
				for (int i = 0; i < model.getJSONObject("Data").getJSONArray("List").length(); i++) {
					JSONObject item = model.getJSONObject("Data").getJSONArray("List").getJSONObject(i);
					if (item.getString("ImageUrl").compareTo("null") != 0 && getLauncherActivity().getUrlImage(item.getString("ImageUrl"), IMG_MAX_DIM_SIZE) == null)
						downloadUrlImage(item.getString("ImageUrl"), getLauncherActivity());
				}

				return (model);
			} catch (Exception exception1) {
				try {
					// prelevo il risultato dell'ultima chiamata andata a buon
					// fine
					JSONObject oldModel = null;
					String strOldModel = (String) getLauncherActivity().readFromDevice("travels" + pageIndex);
					if (strOldModel != null && (genericFilter == null || genericFilter.length() == 0))
						oldModel = new JSONObject(strOldModel);

					return (oldModel);
				} catch (Exception exception2) {
					return (null);
				}
			}
		}

		protected void progress(Object params) {
		}

		protected void afterExecution(Object output) {
			try {
				// nascondo la rotella di attesa
				getTravelsSwrRefresh().setRefreshing(false);
				getLauncherActivity().componentHide(getTravelsPrbWaiting());

				JSONObject model = (JSONObject) output;

				// probabile problema di connessione
				if (model == null)
					getLauncherActivity().alert(getString(R.string.app_errore), getString(R.string.app_problema_connessione));
				else {
					if (pageIndex > model.getJSONObject("Data").getLong("PageCount")) {
						load = false;
						return;
					} else {
						// quando si fa il refresh la lista deve essere svuotata
						if (pageIndex == 1) {
							travelsListAdapter.clear();
							travelsListIds.clear();
						}

						// compilo la lista di viaggi
						for (int i = 0; i < model.getJSONObject("Data").getJSONArray("List").length(); i++) {
							JSONObject item = model.getJSONObject("Data").getJSONArray("List").getJSONObject(i);

							Bitmap image = null;
							if (item.getString("ImageUrl").compareTo("null") != 0)
								image = getLauncherActivity().getUrlImage(item.getString("ImageUrl"), IMG_MAX_DIM_SIZE);
							else
								image = getLauncherActivity().getResImage(R.drawable.no_photo_img, IMG_MAX_DIM_SIZE);

							travelsListIds.add(item.getInt("TravelID"));
							travelsListAdapter.add(new TravelsAdapterDataItem(item.getString("Place"), getLauncherActivity().dateFormat(
									getLauncherActivity().jsonDateParse(item.getString("DepartureDate"))), getLauncherActivity().dateFormat(
									getLauncherActivity().jsonDateParse(item.getString("ArrivalDate"))), image));
						}

						travelsListAdapter.notifyDataSetChanged();

						// gestione del footer di attesa
						if (getTravelsLswTravels().getFooterViewsCount() == 0) {
							getTravelsLswTravels().setAdapter(travelsListAdapter);
							getTravelsLswTravels().addFooterView(travelsListWaiting);
						}

						if (pageIndex >= model.getJSONObject("Data").getLong("PageCount")) {
							load = false;
							getTravelsLswTravels().setAdapter(travelsListAdapter);
							getTravelsLswTravels().removeFooterView(travelsListWaiting);
							getTravelsLswTravels().setSelection(model.getJSONObject("Data").getJSONArray("List").length());
						}

						// se la lista dei risultati � vuota:
						if (model.getJSONObject("Data").getLong("RecordCount") == 0)
							getLauncherActivity().toast(getString(R.string.travels_nessun_risultato));
					}
				}
			} catch (JSONException e) {
				getLauncherActivity().alert(getString(R.string.app_errore), getString(R.string.app_problema_comunicazione));
			}
		}

		protected void onError() {
		}
	}

	private class DeleteTravel extends ParallelTask {
		private int position;

		public DeleteTravel(int position) {
			this.position = position;
		}

		protected void beforeExecution() {
		}

		protected Object execution() {
			try {
				// richiedo al server l'inserimento
				HashMap<String, Object> cmd = new HashMap<String, Object>();
				cmd.put("TravelID", travelsListIds.get(this.position));

				JSONObject model = ApiContext.travelBook().delete("travels/removeTravel", cmd, getLauncherActivity());

				return (model);
			} catch (Exception e) {
				return (null);
			}
		}

		protected void progress(Object params) {
		}

		protected void afterExecution(Object output) {
			try {
				// elaboro la risposta del server
				JSONObject model = (JSONObject) output;
				if (model == null)
					getLauncherActivity().alert(getString(R.string.app_errore), getString(R.string.app_problema_connessione));
				else {
					if (model.getBoolean("Success") == true) {
						// Controllare che sia quello attuale ad essere
						// eliminato
						if (getLauncherActivity().isMyServiceRunning(LocationService.class))
							(new CheckActivateTravel(travelsListIds.get(this.position))).execute();

						getLauncherActivity().activityOpen(MainActivity.class, true);
					} else
						getLauncherActivity().toast(model.getString("ErrorDescription"));
				}
			} catch (JSONException e) {
				getLauncherActivity().alert(getString(R.string.app_errore), getString(R.string.app_problema_comunicazione));
			}
		}

		protected void onError() {
		}
	}

	private class CheckActivateTravel extends ParallelTask {
		private int selectedTravelId;

		public CheckActivateTravel(int selectedTravelId) {
			this.selectedTravelId = selectedTravelId;
		}

		protected void beforeExecution() {
		}

		protected Object execution() {
			try {
				// richiedo al server i viaggi
				HashMap<String, Object> cmd = new HashMap<String, Object>();
				JSONObject model = ApiContext.travelBook().get("travels/getActivateTravel", cmd);

				return (model);
			} catch (Exception exception1) {
				return (null);
			}
		}

		protected void progress(Object params) {
		}

		protected void afterExecution(Object output) {
			try {
				JSONObject model = (JSONObject) output;

				if (model != null) {
					// se c'� un viaggio gi� settato come "in corso"
					if (model.getBoolean("Success") == true) {
						JSONObject item = model.getJSONObject("Data");
						if (selectedTravelId == item.getInt("TravelID"))
							getLauncherActivity().stopService(new Intent(getLauncherActivity().getApplicationContext(), LocationService.class));
					}
				}
			} catch (JSONException e) {
			}
		}

		protected void onError() {
		}
	}
}