package com.intellmeet.tbr;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import org.json.JSONException;
import org.json.JSONObject;

public abstract class ApiClient {
	private final double DEFAULT_TIMEOUT_SECONDS = 12;

	private String apiUrl;
	private HashMap<String, String> cookies;
	private double timeoutSeconds;

	protected abstract boolean check(MasterActivity activity);

	protected abstract void checkFailed(MasterActivity activity);

	public ApiClient(String apiUrl) {
		this.apiUrl = apiUrl;
		timeoutSeconds = DEFAULT_TIMEOUT_SECONDS;
		cookies = new HashMap<String, String>();
	}

	public ApiClient(String apiUrl, double timeoutSeconds) {
		this.apiUrl = apiUrl;
		this.timeoutSeconds = timeoutSeconds;
		cookies = new HashMap<String, String>();
	}

	private String urlParametersRequest(String partialUrl, HashMap<String, Object> command) {
		String stringRequest = apiUrl + "/" + partialUrl;
		if (command != null && command.size() > 0) {
			stringRequest += "?";
			for (String key : command.keySet())
				if (command.get(key) != null)
					stringRequest += (key + "=" + command.get(key).toString()) + "&";

			stringRequest = stringRequest.substring(0, stringRequest.length() - 1);
			stringRequest = stringRequest.replace(" ", "%20");
		}

		return (stringRequest);
	}

	private String hashMapCommandToJson(HashMap<String, Object> hashMapCommand) throws JSONException {
		// dall'HashMap ad un oggetto JSON
		JSONObject jsonCommand = new JSONObject();
		for (String key : hashMapCommand.keySet()) {
			Object value = hashMapCommand.get(key);

			if (value != null) {
				if (value instanceof String)
					jsonCommand.put(key, (String) value);

				if (value instanceof CharSequence)
					jsonCommand.put(key, (String) value.toString());

				if (value instanceof Boolean)
					jsonCommand.put(key, (Boolean) value);

				if (value instanceof Double)
					jsonCommand.put(key, (Double) value);

				if (value instanceof Long)
					jsonCommand.put(key, (Long) value);

				if (value instanceof TimeSpan)
					jsonCommand.put(key, (String) value.toString());

				if (value instanceof Integer)
					jsonCommand.put(key, (Integer) value);

				if (value instanceof Date) {
					Date date = (Date) value;
					SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd", Locale.ITALIAN);
					String strDate = formatter.format(date);

					jsonCommand.put(key, (String) strDate);
				}
			}
		}

		return (jsonCommand.toString());
	}

	private JSONObject inputStreamResponseToJson(InputStream response) throws IOException, JSONException {
		BufferedReader br = new BufferedReader(new InputStreamReader(response, "UTF-8"));

		StringBuffer sb = new StringBuffer();
		String line = null;
		while ((line = br.readLine()) != null)
			sb.append(line);

		JSONObject result = new JSONObject(sb.toString());

		return (result);
	}

	private void setCookies(HttpURLConnection connection) {
		String header = connection.getHeaderFields().toString();

		if (header.contains("Set-Cookie") == true) {
			// prelevo i cookie dall'header
			String cookies = header.substring(header.indexOf("Set-Cookie") + 12, header.length() - 1);
			cookies = cookies.substring(0, cookies.indexOf("]"));
			cookies = cookies.replace("path=/;", "");
			cookies = cookies.replace("HttpOnly,", "");
			cookies = cookies.replace("HttpOnly", "");
			cookies = cookies.replace(" ", "");

			// memorizzo i cookie
			String[] splittedCookies = cookies.split(";|=");
			for (int i = 0; i < splittedCookies.length; i += 2)
				this.cookies.put(splittedCookies[i], splittedCookies[i + 1]);
		}
	}

	private String getCookies() {
		String result = "";
		for (String key : cookies.keySet())
			result += (key + "=" + cookies.get(key) + "; ");

		if (result.length() > 0)
			result = result.substring(0, result.length() - 1);

		return (result);
	}

	public JSONObject get(String partialUrl, HashMap<String, Object> command, MasterActivity activity) throws IOException, JSONException {
		// controllo che precede la richiesta
		if (activity != null)
			if (check(activity) == false) {
				checkFailed(activity);

				return (null);
			}

		// apro la connessione con il server
		URL url = new URL(urlParametersRequest(partialUrl, command));
		HttpURLConnection connection = (HttpURLConnection) url.openConnection();
		connection.setRequestMethod("GET");
		connection.setRequestProperty("Content-Type", "application/json");
		connection.setRequestProperty("Accept", "application/json");
		connection.setRequestProperty("Cookie", getCookies());
		connection.setConnectTimeout((int) (timeoutSeconds * 1000));
		connection.setReadTimeout((int) (timeoutSeconds * 1000));

		// catturo la risposta del server
		JSONObject model = inputStreamResponseToJson(connection.getInputStream());

		// memorizzo i cookie
		setCookies(connection);

		// chiudo la connessione
		connection.disconnect();

		return (model);
	}

	public JSONObject get(String partialUrl, HashMap<String, Object> command) throws IOException, JSONException {
		return (get(partialUrl, command, null));
	}

	public JSONObject post(String partialUrl, HashMap<String, Object> command, MasterActivity activity) throws IOException, JSONException {
		// controllo che precede la richiesta
		if (activity != null)
			if (check(activity) == false) {
				checkFailed(activity);

				return (null);
			}

		// apro la connessione con il server
		URL url = new URL(apiUrl + "/" + partialUrl);
		HttpURLConnection connection = (HttpURLConnection) url.openConnection();
		connection.setRequestMethod("POST");
		connection.setRequestProperty("Content-Type", "application/json");
		connection.setRequestProperty("Accept", "application/json");
		connection.setRequestProperty("Cookie", getCookies());
		connection.setConnectTimeout((int) (timeoutSeconds * 1000));
		connection.setReadTimeout((int) (timeoutSeconds * 1000));

		// invio il command
		BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(connection.getOutputStream(), "UTF-8"));
		bufferedWriter.write(hashMapCommandToJson(command));
		bufferedWriter.flush();
		bufferedWriter.close();

		// catturo la risposta del server
		JSONObject model = inputStreamResponseToJson(connection.getInputStream());

		// memorizzo i cookie
		setCookies(connection);

		// chiudo la connessione
		connection.disconnect();

		return (model);
	}

	public JSONObject post(String partialUrl, HashMap<String, Object> command) throws IOException, JSONException {
		return (post(partialUrl, command, null));
	}

	public JSONObject put(String partialUrl, HashMap<String, Object> command, MasterActivity activity) throws IOException, JSONException {
		// controllo che precede la richiesta
		if (activity != null)
			if (check(activity) == false) {
				checkFailed(activity);

				return (null);
			}

		// apro la connessione con il server
		URL url = new URL(apiUrl + "/" + partialUrl);
		HttpURLConnection connection = (HttpURLConnection) url.openConnection();
		connection.setRequestMethod("PUT");
		connection.setRequestProperty("Content-Type", "application/json");
		connection.setRequestProperty("Accept", "application/json");
		connection.setRequestProperty("Cookie", getCookies());
		connection.setConnectTimeout((int) (timeoutSeconds * 1000));
		connection.setReadTimeout((int) (timeoutSeconds * 1000));

		// invio il command
		BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(connection.getOutputStream(), "UTF-8"));
		bufferedWriter.write(hashMapCommandToJson(command));
		bufferedWriter.flush();
		bufferedWriter.close();

		// catturo la risposta del server
		JSONObject model = inputStreamResponseToJson(connection.getInputStream());

		// memorizzo i cookie
		setCookies(connection);

		// chiudo la connessione
		connection.disconnect();

		return (model);
	}

	public JSONObject put(String partialUrl, HashMap<String, Object> command) throws IOException, JSONException {
		return (put(partialUrl, command, null));
	}

	public JSONObject delete(String partialUrl, HashMap<String, Object> command, MasterActivity activity) throws IOException, JSONException {
		// controllo che precede la richiesta
		if (activity != null)
			if (check(activity) == false) {
				checkFailed(activity);

				return (null);
			}

		// apro la connessione con il server
		URL url = new URL(urlParametersRequest(partialUrl, command));
		HttpURLConnection connection = (HttpURLConnection) url.openConnection();
		connection.setRequestMethod("DELETE");
		connection.setRequestProperty("Content-Type", "application/json");
		connection.setRequestProperty("Accept", "application/json");
		connection.setRequestProperty("Cookie", getCookies());
		connection.setConnectTimeout((int) (timeoutSeconds * 1000));
		connection.setReadTimeout((int) (timeoutSeconds * 1000));

		// catturo la risposta del server
		JSONObject model = inputStreamResponseToJson(connection.getInputStream());

		// memorizzo i cookie
		setCookies(connection);

		// chiudo la connessione
		connection.disconnect();

		return (model);
	}

	public JSONObject delete(String partialUrl, HashMap<String, Object> command) throws IOException, JSONException {
		return (delete(partialUrl, command, null));
	}
}