package com.intellmeet.tbr.activities;

import java.util.HashMap;

import org.json.JSONException;
import org.json.JSONObject;

import com.intellmeet.tbr.activities.MainActivity;
import com.intellmeet.tbr.ParallelTask;
import com.intellmeet.tbr.MasterActivity;
import com.intellmeet.tbr.R;
import com.intellmeet.tbr.http.ApiContext;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class AccessActivity extends MasterActivity {
	private final int VIEW_STATE_LOGIN = 0;
	private final int VIEW_STATE_SIGN_UP = 1;
	private final int VIEW_STATE_PSW_RECOVERY = 2;

	
	private EditText getLoginTxtEmail() {
		return ((EditText) findViewById(R.id.loginTxtEmail));
	}

	private EditText getLoginTxtPassword() {
		return ((EditText) findViewById(R.id.loginTxtPassword));
	}

	private Button getLoginBtnLogin() {
		return ((Button) findViewById(R.id.loginBtnLogin));
	}

	private TextView getLoginLblSignUp() {
		return ((TextView) findViewById(R.id.loginLblSignUp));
	}

	private TextView getLoginLblAppName() {
		return ((TextView) findViewById(R.id.loginLblAppName));
	}

	private TextView getSignUpLblAppName() {
		return ((TextView) findViewById(R.id.signUpLblAppName));
	}

	private EditText getSignUpTxtEmail() {
		return ((EditText) findViewById(R.id.signUpTxtEmail));
	}

	private EditText getSignUpTxtPassword() {
		return ((EditText) findViewById(R.id.signUpTxtPassword));
	}

	private EditText getSignUpTxtPasswordConfirm() {
		return ((EditText) findViewById(R.id.signUpTxtPasswordConfirm));
	}

	private Button getSignUpBtnSignUp() {
		return ((Button) findViewById(R.id.signUpBtnSignUp));
	}

	private TextView getSignUpLblLogin() {
		return ((TextView) findViewById(R.id.signUpLblLogin));
	}

	private TextView getLoginLblPasswordRecovery() {
		return ((TextView) findViewById(R.id.loginLblPasswordRecovery));
	}

	private TextView getPswRecoveryLblAppName() {
		return ((TextView) findViewById(R.id.pswRecoveryLblAppName));
	}

	private TextView getPswRecoveryLblLogin() {
		return ((TextView) findViewById(R.id.pswRecoveryLblLogin));
	}

	private EditText getPswRecoveryTxtEmail() {
		return ((EditText) findViewById(R.id.pswRecoveryTxtEmail));
	}

	private Button getPswRecoveryBtnNewPassword() {
		return ((Button) findViewById(R.id.pswRecoveryBtnNewPassword));
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// se in precedenza l'utente si � gi� loggato passo direttamente
		// sull'activity principale
		if (readFromDevice("email") != null && readFromDevice("password") != null)
			activityOpen(MainActivity.class, true);

		setAlertTheme(AlertDialog.THEME_HOLO_DARK);
		setDatePickerTheme(DatePickerDialog.THEME_HOLO_DARK);
        
		switch (getViewState()) {
		case VIEW_STATE_LOGIN:
			loginPageShow();

			break;

		case VIEW_STATE_SIGN_UP:
			signUpPageShow();

			break;

		case VIEW_STATE_PSW_RECOVERY:
			pswRecoveryPageShow();
			break;

		default:
			loginPageShow();
			break;
		}
	}

	private void pswRecoveryPageShow() {
		setViewState(VIEW_STATE_PSW_RECOVERY);

		// esposizione della page
		getActionBar().hide();
		setContentView(R.layout.psw_recovery_page);

		// sistemazione della label con il nome dell'app
		setFont(getPswRecoveryLblAppName(), "AppName");
		setTop(getPswRecoveryLblAppName(), 140);
		if (isLandscapeOrientation())
			setTop(getPswRecoveryLblAppName(), 2);

		// posizionamento del campo e-mail
		setTopFromComponent(getPswRecoveryLblAppName(), getPswRecoveryTxtEmail(), 140);
		if (isLandscapeOrientation())
			setTopFromComponent(getPswRecoveryLblAppName(), getPswRecoveryTxtEmail(), 5);

		// posizionamento del pulsante per la generazione di una nuova password
		setTopFromComponent(getPswRecoveryTxtEmail(), getPswRecoveryBtnNewPassword(), 30);
		if (isLandscapeOrientation())
			setTopFromComponent(getPswRecoveryTxtEmail(), getPswRecoveryBtnNewPassword(), 15);

		// sistemazione della label che riporta sul login
		underline(getPswRecoveryLblLogin());
		setBottom(getPswRecoveryLblLogin(), 40);
		if (isLandscapeOrientation())
			setBottom(getPswRecoveryLblLogin(), 15);
	}

	private void loginPageShow() {
		setViewState(VIEW_STATE_LOGIN);

		// esposizione della page
		getActionBar().hide();
		setContentView(R.layout.login_page);

		// sistemazione della label con il nome dell'app
		setFont(getLoginLblAppName(), "AppName");
		setTop(getLoginLblAppName(), 140);
		if (isLandscapeOrientation())
			setTop(getLoginLblAppName(), 2);

		// posizionamento del campo e-mail
		setTopFromComponent(getLoginLblAppName(), getLoginTxtEmail(), 140);
		if (isLandscapeOrientation())
			setTopFromComponent(getLoginLblAppName(), getLoginTxtEmail(), 10);

		// posizionamento del campo password
		setTopFromComponent(getLoginTxtEmail(), getLoginTxtPassword(), 30);
		if (isLandscapeOrientation())
			setTopFromComponent(getLoginTxtEmail(), getLoginTxtPassword(), 15);

		// posizionamento del pulsante per il login
		setTopFromComponent(getLoginTxtPassword(), getLoginBtnLogin(), 30);
		if (isLandscapeOrientation())
			setTopFromComponent(getLoginTxtPassword(), getLoginBtnLogin(), 15);

		// sistemazione della label per il recupero della password
		underline(getLoginLblPasswordRecovery());
		setTopFromComponent(getLoginBtnLogin(), getLoginLblPasswordRecovery(), 20);
		if (isLandscapeOrientation())
			setTopFromComponent(getLoginBtnLogin(), getLoginLblPasswordRecovery(), 15);

		// sistemazione della label per la registrazione
		underline(getLoginLblSignUp());
		setBottom(getLoginLblSignUp(), 40);
		if (isLandscapeOrientation())
			setBottom(getLoginLblSignUp(), 15);

		// qualche eccezione per gli schermi pi� piccoli
		loginPageCompatibility();
	}

	private void signUpPageShow() {
		setViewState(VIEW_STATE_SIGN_UP);

		getActionBar().hide();
		setContentView(R.layout.sign_up_page);

		// sistemazione della label con il nome dell'app
		setTop(getSignUpLblAppName(), 140);
		setFont(getSignUpLblAppName(), "AppName");

		// posizionamento del campo e-mail
		setTopFromComponent(getSignUpLblAppName(), getSignUpTxtEmail(), 140);

		// posizionamento del campo password
		setTopFromComponent(getSignUpTxtEmail(), getSignUpTxtPassword(), 30);

		// posizionamento del campo di conferma della password
		setTopFromComponent(getSignUpTxtPassword(), getSignUpTxtPasswordConfirm(), 20);

		// sistemazione del bottone signup
		setTopFromComponent(getSignUpTxtPasswordConfirm(), getSignUpBtnSignUp(), 35);
		underline(getSignUpLblLogin());

		// sistemazione della label che riporta sul login
		setTopFromComponent(getSignUpBtnSignUp(), getSignUpLblLogin(), 50);
		underline(getSignUpLblLogin());
	}

	private void loginPageCompatibility() {
		if (getScreenSizeVal() == 2 && isLandscapeOrientation() == false) {
			setTop(getLoginLblAppName(), 120);
			setTopFromComponent(getLoginLblAppName(), getLoginTxtEmail(), 80);
		}

		if (getScreenSizeVal() < 2)
			if (isLandscapeOrientation() == false) {
				setTop(getLoginLblAppName(), 60);
				setTopFromComponent(getLoginLblAppName(), getLoginTxtEmail(), 40);
				setBottom(getLoginLblSignUp(), 30);
			} else {
				getLoginLblAppName().setTextSize(60);
				setTop(getLoginLblAppName(), 0);
				setTopFromComponent(getLoginLblAppName(), getLoginTxtEmail(), 0);
				setTopFromComponent(getLoginTxtEmail(), getLoginTxtPassword(), 5);
				setTopFromComponent(getLoginTxtPassword(), getLoginBtnLogin(), 5);
				setBottom(getLoginLblSignUp(), 10);
			}
	}

	public void loginBtnLogin_onClick(View view) {
		if (getLoginTxtEmail().getText().toString().equals("")) {
			Toast.makeText(AccessActivity.this, R.string.access_inserire_email, Toast.LENGTH_SHORT).show();
			return;
		} else {
			if (getLoginTxtPassword().getText().toString().equals("")) {
				Toast.makeText(AccessActivity.this, R.string.access_inserire_password, Toast.LENGTH_SHORT).show();
				return;
			} else {
				(new Login()).execute();
			}
		}
	}

	public void loginLblSignUp_onClick(View view) {
		signUpPageShow();
	}

	public void signUpLblLogin_onClick(View view) {
		loginPageShow();
	}

	public void loginLblPasswordRecovery_onClick(View view) {
		pswRecoveryPageShow();
	}

	public void pswRecoveryLblLogin_onClick(View view) {
		loginPageShow();
	}

	public void pswRecoveryBtnNewPassword_onClick(View view) {
		if (getPswRecoveryTxtEmail().getText().toString() == null || getPswRecoveryTxtEmail().getText().toString().length() == 0)
			toast(getString(R.string.access_inserire_email));
		else
			(new NewPassword()).execute();
	}

	public void signUpBtnSignUp_onClick(View view) {
		if (!getSignUpTxtPassword().getText().toString().equals("") && !getSignUpTxtPasswordConfirm().getText().toString().equals("") && !getSignUpTxtEmail().getText().toString().equals("")) {
			if (getSignUpTxtPassword().getText().toString().compareTo(getSignUpTxtPasswordConfirm().getText().toString()) != 0)
				toast(getString(R.string.access_errore_conferma_password));
			else
				(new SignUp()).execute();
		} else {
			toast(getString(R.string.access_inserire_credenziali));
		}
	}

	private class NewPassword extends ParallelTask {
		protected void beforeExecution() {
			// durante l'attesa disattivo la rotazione
			rotationLock();

			// disattivo anche il pulsante
			getPswRecoveryBtnNewPassword().setEnabled(false);
			getPswRecoveryBtnNewPassword().setText(getString(R.string.app_attendere));
		}

		protected Object execution() {
			try {
				// richiedo la generazione di una nuova password
				HashMap<String, Object> cmd = new HashMap<String, Object>();
				cmd.put("Email", getPswRecoveryTxtEmail().getText());
				JSONObject model = ApiContext.travelBook().put("users/newRandomPassword", cmd);

				return (model);
			} catch (Exception e) {
				return (null);
			}
		}

		protected void progress(Object params) {
		}

		protected void afterExecution(Object output) {
			try {
				// riabilito il pulsante
				getPswRecoveryBtnNewPassword().setEnabled(true);
				getPswRecoveryBtnNewPassword().setText(getString(R.string.access_genera_nuova_password));

				// riattivo la rotazione
				rotationUnlock();

				// resetto la password
				getPswRecoveryTxtEmail().setText("");

				// elaboro la risposta del server
				JSONObject model = (JSONObject) output;
				if (model == null)
					alert(getString(R.string.app_errore), getString(R.string.app_problema_connessione));
				else {
					if (model.getBoolean("Success") == true) {
						alert(getString(R.string.app_attenzione), getString(R.string.access_inviata_email_nuova_password));

						loginPageShow();
					} else
						toast(getString(R.string.access_email_errata));
				}
			} catch (JSONException e) {
				alert(getString(R.string.app_errore), getString(R.string.app_problema_comunicazione));
			}
		}

		protected void onError() {
			rotationUnlock();
		}
	}

	private class SignUp extends ParallelTask {
		protected void beforeExecution() {
			// durante l'attesa disattivo la rotazione
			rotationLock();

			// disattivo anche il pulsante
			getSignUpBtnSignUp().setEnabled(false);
			getSignUpBtnSignUp().setText(getString(R.string.app_attendere));
		}

		protected Object execution() {
			try {
				// richiedo al server la registrazione
				HashMap<String, Object> cmd = new HashMap<String, Object>();
				cmd.put("Email", getSignUpTxtEmail().getText());
				cmd.put("Password", getSignUpTxtPassword().getText());
				JSONObject model = ApiContext.travelBook().post("users/addUser", cmd);

				return (model);
			} catch (Exception e) {
				return (null);
			}
		}

		protected void progress(Object params) {
		}

		protected void afterExecution(Object output) {
			try {
				// riabilito il pulsante di registrazione
				getSignUpBtnSignUp().setEnabled(true);
				getSignUpBtnSignUp().setText(getString(R.string.access_registrati));

				// riattivo la rotazione
				rotationUnlock();

				// elaboro la risposta del server
				JSONObject model = (JSONObject) output;
				if (model == null)
					alert(getString(R.string.app_errore), getString(R.string.app_problema_connessione));
				else {
					if (model.getBoolean("Success") == true) {
						// messaggio di conferma
						alert(getString(R.string.app_attenzione), getString(R.string.access_iscrizione_avvenuta));

						// ritorno al login
						loginPageShow();
					} else
						toast(model.getString("ErrorDescription").toString());
				}
			} catch (JSONException e) {
				alert(getString(R.string.app_errore), getString(R.string.app_problema_comunicazione));
			}
		}

		protected void onError() {
			rotationUnlock();
		}
	}

	private class Login extends ParallelTask {
		protected void beforeExecution() {
			// durante l'attesa disattivo la rotazione
			rotationLock();

			// disattivo anche il pulsante
			getLoginBtnLogin().setEnabled(false);
			getLoginBtnLogin().setText(getString(R.string.app_attendere));
		}

		protected Object execution() {
			try {
				// richiedo l'autenticazione al server
				HashMap<String, Object> cmd = new HashMap<String, Object>();
				cmd.put("Email", getLoginTxtEmail().getText());
				cmd.put("Password", getLoginTxtPassword().getText());
				JSONObject model = ApiContext.travelBook().get("session/login", cmd);

				return (model);
			} catch (Exception e) {
				return (null);
			}
		}

		protected void progress(Object params) {
		}

		protected void afterExecution(Object output) {
			try {
				// riabilito il pulsante di login
				getLoginBtnLogin().setEnabled(true);
				getLoginBtnLogin().setText(getString(R.string.access_login));

				// riattivo la rotazione
				rotationUnlock();

				// elaboro la risposta del server
				JSONObject model = (JSONObject) output;
				if (model == null) {
					alert(getString(R.string.app_errore), getString(R.string.app_problema_connessione));
				} else {
					if (model.getBoolean("Data") == true) {
						// salvo le credenziali
						saveToDevice("email", getLoginTxtEmail().getText().toString());
						saveToDevice("password", getLoginTxtPassword().getText().toString());

						// apro una nuova activity chiudendo la corrente
						activityOpen(MainActivity.class, true);
					} else {
						toast(getString(R.string.access_login_fallito));
						// resetto la password
						getLoginTxtPassword().setText("");
					}
				}
			} catch (JSONException e) {
				alert(getString(R.string.app_errore), getString(R.string.app_problema_comunicazione));
			}
		}

		protected void onError() {
			rotationUnlock();
		}
	}
}
