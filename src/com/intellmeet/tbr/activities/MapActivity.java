package com.intellmeet.tbr.activities;

import java.util.HashMap;

import org.json.JSONException;
import org.json.JSONObject;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.MenuItem;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.intellmeet.tbr.MasterActivity;
import com.intellmeet.tbr.ParallelTask;
import com.intellmeet.tbr.R;
import com.intellmeet.tbr.http.ApiContext;

public class MapActivity extends MasterActivity implements OnMapReadyCallback {

	private GoogleMap googleMap;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.map_frame);

		// settaggio della action bar
		ColorDrawable colorDrawable = new ColorDrawable(Color.parseColor("#06A1A4"));
		getActionBar().setBackgroundDrawable(colorDrawable);
		getActionBar().setIcon(R.drawable.ic_left_arrow_w);
		getActionBar().setTitle(getString(R.string.tag_indietro));
		getActionBar().setHomeButtonEnabled(true);

		MapFragment mapFragment = ((MapFragment) getFragmentManager().findFragmentById(R.id.map));
		mapFragment.getMapAsync(this);
	}

	public boolean onOptionsItemSelected(MenuItem menuItem) {
		boolean result = super.onOptionsItemSelected(menuItem);
		finish();
		return (result);
	}

	private class GetCoordinates extends ParallelTask {
		protected void beforeExecution() {
		}

		protected Object execution() {
			try {
				// richiedo al server i viaggi (in maniera paginata)
				HashMap<String, Object> cmd = new HashMap<String, Object>();
				cmd.put("TravelID", getActivityParam("TravelID"));
				JSONObject model = ApiContext.travelBook().get("tags/getCoordinates", cmd);

				saveToDevice("coordinates" + getActivityParam("TravelID"), model.toString());

				return (model);
			} catch (Exception exception1) {
				try {
					// prelevo il risultato dell'ultima chiamata andata a buon
					// fine
					JSONObject oldModel = null;
					String strOldModel = (String) readFromDevice("coordinates" + getActivityParam("TravelID"));
					oldModel = new JSONObject(strOldModel);

					return (oldModel);
				} catch (Exception exception2) {
					return (null);
				}
			}
		}

		protected void progress(Object params) {
		}

		protected void afterExecution(Object output) {
			try {
				JSONObject model = (JSONObject) output;

				// probabile problema di connessione
				if (model == null)
					alert(getString(R.string.app_errore), getString(R.string.app_problema_connessione));
				else {
					// se la lista dei risultati � vuota:
					if (model.getJSONObject("Data").getLong("RecordCount") == 0) {
						toast(getString(R.string.tag_nessun_risultato));
					} else {
						if (model.getJSONObject("Data").getJSONArray("List").length() > 1) {
							int middle = (model.getJSONObject("Data").getJSONArray("List").length() / 2);
							int count = 0;

							// compilo la lista di viaggi
							for (int i = 0; i < model.getJSONObject("Data").getJSONArray("List").length(); i++) {
								JSONObject item = model.getJSONObject("Data").getJSONArray("List").getJSONObject(i);
								googleMap.addMarker(new MarkerOptions().position(new LatLng(item.getDouble("Latitude"), item.getDouble("Longitude"))).title(item.getString("Name"))
										.snippet(dateTimeFormat(jsonDateParse(item.getString("Date")))));
								count++;
								if (count == middle)
									// Move the camera instantly to the middle
									// with
									// a zoom of 10.
									googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(item.getDouble("Latitude"), item.getDouble("Longitude")), 10));
							}
						} else {
							// compilo la lista di viaggi
							for (int i = 0; i < model.getJSONObject("Data").getJSONArray("List").length(); i++) {
								JSONObject item = model.getJSONObject("Data").getJSONArray("List").getJSONObject(i);
								googleMap.addMarker(new MarkerOptions().position(new LatLng(item.getDouble("Latitude"), item.getDouble("Longitude"))).title(item.getString("Name"))
										.snippet(dateTimeFormat(jsonDateParse(item.getString("Date")))));

								// Move the camera instantly to the middle
								// with
								// a zoom of 10.
								googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(item.getDouble("Latitude"), item.getDouble("Longitude")), 10));
							}
						}
					}
				}
			} catch (JSONException e) {
				alert(getString(R.string.app_errore), getString(R.string.app_problema_comunicazione));
			}
		}

		protected void onError() {
		}
	}

	private class GetCoordinate extends ParallelTask {
		protected void beforeExecution() {
		}

		protected Object execution() {
			try {
				// richiedo al server i viaggi (in maniera paginata)
				HashMap<String, Object> cmd = new HashMap<String, Object>();
				cmd.put("TagID", getActivityParam("TagID"));
				JSONObject model = ApiContext.travelBook().get("tags/getTag", cmd);

				saveToDevice("coordinate" + getActivityParam("TagID"), model.toString());

				return (model);
			} catch (Exception exception1) {
				try {
					// prelevo il risultato dell'ultima chiamata andata a buon
					// fine
					JSONObject oldModel = null;
					String strOldModel = (String) readFromDevice("coordinate" + getActivityParam("TagID"));
					oldModel = new JSONObject(strOldModel);

					return (oldModel);
				} catch (Exception exception2) {
					return (null);
				}
			}
		}

		protected void progress(Object params) {
		}

		protected void afterExecution(Object output) {
			try {
				JSONObject model = (JSONObject) output;

				// probabile problema di connessione
				if (model == null)
					toast(getString(R.string.travels_nessun_risultato));
				else {
					JSONObject item = model.getJSONObject("Data");
					googleMap.addMarker(new MarkerOptions().position(new LatLng(item.getDouble("Latitude"), item.getDouble("Longitude"))).title(item.getString("Name"))
							.snippet(dateTimeFormat(jsonDateParse(item.getString("Date")))));

					// Move the camera instantly to the middle with
					// a zoom of 10.
					googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(item.getDouble("Latitude"), item.getDouble("Longitude")), 15));
				}
			} catch (JSONException e) {
				alert(getString(R.string.app_errore), getString(R.string.app_problema_comunicazione));
			}
		}

		protected void onError() {
		}
	}

	@Override
	public void onMapReady(GoogleMap map) {
		googleMap = map;
		setUpMap();
	}

	public void setUpMap() {
		googleMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
		googleMap.setMyLocationEnabled(true);
		googleMap.setTrafficEnabled(true);
		googleMap.setIndoorEnabled(true);
		googleMap.setBuildingsEnabled(true);
		googleMap.getUiSettings().setZoomControlsEnabled(true);

		if (getActivityParam("TravelID") != null && getActivityParam("TagID") == null)
			(new GetCoordinates()).execute();

		if (getActivityParam("TravelID") == null && getActivityParam("TagID") != null)
			(new GetCoordinate()).execute();

	}
}
