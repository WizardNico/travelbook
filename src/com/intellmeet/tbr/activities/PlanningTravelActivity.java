package com.intellmeet.tbr.activities;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.intellmeet.tbr.MasterActivity;
import com.intellmeet.tbr.R;
import com.intellmeet.tbr.ParallelTask;
import com.intellmeet.tbr.http.ApiContext;
import com.intellmeet.tbr.services.LocationService;

public class PlanningTravelActivity extends MasterActivity {

	private EditText getAddTravelTxtTitle() {
		return ((EditText) findViewById(R.id.addTravelTxtTitle));
	}

	private ImageView getAddTravelImgDepartureDate() {
		return ((ImageView) findViewById(R.id.addTravelImgDepartureDate));
	}

	private EditText getAddTravelTxtDepartureDate() {
		return ((EditText) findViewById(R.id.addTravelTxtDepartureDate));
	}

	private ImageView getAddTravelImgArrivalDate() {
		return ((ImageView) findViewById(R.id.addTravelImgArrivalDate));
	}

	private EditText getAddTravelTxtArrivalDate() {
		return ((EditText) findViewById(R.id.addTravelTxtArrivalDate));
	}

	private Button getAddTravelBtnAdd() {
		return ((Button) findViewById(R.id.addTravelBtnAdd));
	}

	protected void onCreate(Bundle bundle) {
		super.onCreate(bundle);

		setWaitingTheme(ProgressDialog.THEME_HOLO_DARK);
		setAlertTheme(AlertDialog.THEME_HOLO_DARK);
		setDatePickerTheme(DatePickerDialog.THEME_HOLO_DARK);
		setTimePickerTheme(TimePickerDialog.THEME_HOLO_DARK);

		if (getActivityParam("Id") == null)
			addTravelPageShow();
	}

	public boolean onCreateOptionsMenu(Menu menu) {
		return (true);
	}

	private void editTravelPageShow() {
		// aggiornamento dei pulsanti sulla action bar
		invalidateOptionsMenu();
	}

	private void addTravelPageShow() {
		// espongo la page
		setContentView(R.layout.add_travel_page);

		// settaggio della action bar
		ColorDrawable colorDrawable = new ColorDrawable(Color.parseColor("#06A1A4"));
		getActionBar().setBackgroundDrawable(colorDrawable);
		getActionBar().setIcon(R.drawable.ic_left_arrow_w);
		getActionBar().setTitle(getString(R.string.travel_indietro));
		getActionBar().setHomeButtonEnabled(true);

		// posizionamento dei componenti
		setTop(getAddTravelTxtTitle(), 100);
		setTopFromComponent(getAddTravelTxtTitle(), getAddTravelTxtDepartureDate(), 100);
		setTopFromComponent(getAddTravelTxtTitle(), getAddTravelImgDepartureDate(), 95);
		setTopFromComponent(getAddTravelTxtDepartureDate(), getAddTravelTxtArrivalDate(), 50);
		setTopFromComponent(getAddTravelTxtDepartureDate(), getAddTravelImgArrivalDate(), 45);
		setTopFromComponent(getAddTravelImgArrivalDate(), getAddTravelBtnAdd(), 100);
	}

	public boolean onOptionsItemSelected(MenuItem menuItem) {
		boolean result = super.onOptionsItemSelected(menuItem);

		if (menuItem.getItemId() == R.id._editMenuTagCollection) {
			editTravelPageShow();

			return (result);
		}

		finish();
		return (result);
	}

	public void addTravelBtnAdd_onClick(View view) {
		if (getAddTravelTxtArrivalDate().getText().toString().equals("") || getAddTravelTxtDepartureDate().getText().toString().equals("")) {
			toast("Inserire entrambe le date");
		} else {
			if (checkCurrentDate(toDate(getAddTravelTxtDepartureDate())) && checkCurrentDate(toDate(getAddTravelTxtArrivalDate())))
				(new AddTravel()).execute();
			else
				toast("Correggere Le Date");
		}
	}

	public void addTravelImgDepartureDate_onClick(View view) {
		dateSelect(getAddTravelTxtDepartureDate());
	}

	public void addTravelImgArrivalDate_onClick(View view) {
		if (toDate(getAddTravelTxtDepartureDate()) != null)
			dateSelectFromSelectedDate(getAddTravelTxtArrivalDate(), toDate(getAddTravelTxtDepartureDate()));
		else
			dateSelect(getAddTravelTxtArrivalDate());
	}

	private class AddTravel extends ParallelTask {
		private String travelDate;

		protected void beforeExecution() {
			// durante l'attesa disattivo la rotazione
			rotationLock();

			// disattivo anche il pulsante
			getAddTravelBtnAdd().setEnabled(false);
			getAddTravelBtnAdd().setText(getString(R.string.app_attendere));
		}

		protected Object execution() {
			try {
				// richiedo al server l'inserimento
				HashMap<String, Object> cmd = new HashMap<String, Object>();
				cmd.put("Title", getAddTravelTxtTitle().getText());
				cmd.put("DepartureDate", toDate(getAddTravelTxtDepartureDate()));
				cmd.put("ArrivalDate", toDate(getAddTravelTxtArrivalDate()));

				Date todayD = new Date();
				Date travelD = toDate(getAddTravelTxtDepartureDate());

				SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

				String todayDate = sdf.format(todayD);
				travelDate = sdf.format(travelD);

				if (todayDate.equals(travelDate))
					cmd.put("Activate", true);
				else
					cmd.put("Activate", false);

				JSONObject model = ApiContext.travelBook().post("travels/addTravel", cmd, PlanningTravelActivity.this);

				return (model);
			} catch (Exception e) {
				return (null);
			}
		}

		protected void progress(Object params) {
		}

		protected void afterExecution(Object output) {
			try {
				// riabilito il pulsante di inserimento
				getAddTravelBtnAdd().setEnabled(true);
				getAddTravelBtnAdd().setText(getString(R.string.travel_aggiungi));

				// riattivo la rotazione
				rotationUnlock();

				// elaboro la risposta del server
				JSONObject model = (JSONObject) output;
				if (model == null)
					alert(getString(R.string.app_errore), getString(R.string.app_problema_connessione));
				else {
					if (model.getBoolean("Success") == true) {
						setResult(RESULT_OK);
						(new CheckActivateTravel(travelDate)).execute();
						finish();
					} else
						toast(model.getString("ErrorDescription"));
				}
			} catch (JSONException e) {
				alert(getString(R.string.app_errore), getString(R.string.app_problema_comunicazione));
			}
		}

		protected void onError() {
		}
	}

	private class CheckActivateTravel extends ParallelTask {
		private String travelDate;

		public CheckActivateTravel(String travelDate) {
			this.travelDate = travelDate;
		}

		protected void beforeExecution() {
		}

		protected Object execution() {
			try {
				// richiedo al server i viaggi
				HashMap<String, Object> cmd = new HashMap<String, Object>();
				JSONObject model = ApiContext.travelBook().get("travels/getActivateTravel", cmd);

				return (model);
			} catch (Exception exception1) {
				return (null);
			}
		}

		protected void progress(Object params) {
		}

		protected void afterExecution(Object output) {
			try {
				JSONObject model = (JSONObject) output;

				// probabile problema di connessione
				if (model == null)
					alert(getString(R.string.app_errore), getString(R.string.app_problema_connessione));
				else {
					// se c'� un viaggio gi� settato come "in corso"
					if (model.getBoolean("Success") == true) {
						JSONObject item = model.getJSONObject("Data");

						// Prendo la data odierna
						Date todayD = new Date();

						// Le formatto
						SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
						String todayDate = sdf.format(todayD);

						// Controllo che non siano le stesse, ovvero parta oggi
						// stesso
						if (todayDate.equals(travelDate))
							sendStartNotification(" " + item.getString("Place") + " ");

						if (!isMyServiceRunning(LocationService.class))
							startService(new Intent(PlanningTravelActivity.this, LocationService.class));
						else {
							stopService(new Intent(PlanningTravelActivity.this, LocationService.class));
							startService(new Intent(PlanningTravelActivity.this, LocationService.class));
						}
					}
				}
			} catch (JSONException e) {
				alert(getString(R.string.app_errore), getString(R.string.app_problema_comunicazione));
			}
		}

		protected void onError() {
		}
	}
}