package com.intellmeet.tbr.activities;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONException;
import org.json.JSONObject;

import com.intellmeet.tbr.Save;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.util.Base64;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.AdapterView.OnItemClickListener;

import com.intellmeet.tbr.ParallelTask;
import com.intellmeet.tbr.activities.AccessActivity;
import com.intellmeet.tbr.http.ApiContext;
import com.intellmeet.tbr.services.SelectorService;
import com.intellmeet.tbr.adapters.MenuAdapter;
import com.intellmeet.tbr.adapters.dataitems.MenuAdapterDataItem;
import com.intellmeet.tbr.fragments.TravelsFragment;
import com.intellmeet.tbr.R;
import com.intellmeet.tbr.MasterActivity;

public class MainActivity extends MasterActivity {
	private final int PAGE_REFRESH = 3;
	private final int REQUEST_IMAGE_CAPTURE = 1;
	private final int REQUEST_IMAGE_GALLERY = 2;
	private Integer selectedTravel;
	private Uri uri;

	private ListView getMainLswMenuChoices() {
		return ((ListView) findViewById(R.id.mainLswMenuChoices));
	}

	private DrawerLayout getMainDrlMenu() {
		return ((DrawerLayout) findViewById(R.id.mainDrlMenu));
	}

	protected void onCreate(Bundle bundle) {
		super.onCreate(bundle);

		setAlertTheme(AlertDialog.THEME_HOLO_DARK);
		setWaitingTheme(ProgressDialog.THEME_HOLO_DARK);

		if (!isMyServiceRunning(SelectorService.class))
			startService(new Intent(this.getApplicationContext(), SelectorService.class));

		mainPageShow();
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
	}

	private void mainPageShow() {
		// espongo il layout
		setContentView(R.layout.main_page);

		// settaggio della action bar
		ColorDrawable colorDrawable = new ColorDrawable(Color.parseColor("#06A1A4"));
		getActionBar().setBackgroundDrawable(colorDrawable);
		getActionBar().setIcon(R.drawable.ic_three_lines_w);
		getActionBar().setTitle(getString(R.string.main_menu));
		getActionBar().setHomeButtonEnabled(true);

		// creo il menu
		ArrayList<MenuAdapterDataItem> menuAdapterDataItems = new ArrayList<MenuAdapterDataItem>();
		menuAdapterDataItems.add(new MenuAdapterDataItem(getResImage(R.drawable.ic_action_social_public), getString(R.string.main_viaggi)));
		menuAdapterDataItems.add(new MenuAdapterDataItem(getResImage(R.drawable.ic_action_maps_map), getString(R.string.main_crea_viaggio)));
		menuAdapterDataItems.add(new MenuAdapterDataItem(getResImage(R.drawable.ic_exit_arrow_w), getString(R.string.main_logout)));
		MenuAdapter menuAdapter = new MenuAdapter(menuAdapterDataItems, this);
		getMainLswMenuChoices().setAdapter(menuAdapter);

		// attivo la selezione sul menu
		getMainLswMenuChoices().setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
				if (position == 0)
					fragmentOpen(TravelsFragment.class, R.id.mainFrmSelectedMenuChoice);

				if (position == 1) {
					HashMap<String, Object> params = new HashMap<String, Object>();
					params.put("Id", null);
					activityOpenForResult(PlanningTravelActivity.class, params, PAGE_REFRESH);
				}

				if (position == 2)
					confirmAlert(getString(R.string.app_attenzione), getString(R.string.main_conferma_logout), new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int which) {
							(new Logout()).execute();
						}
					}, null);

				if (position != 2)
					getMainDrlMenu().closeDrawers();
			}
		});

		// default per la voce di menu selezionata
		if (isThereFragment(R.id.mainFrmSelectedMenuChoice) == false)
			fragmentOpen(TravelsFragment.class, R.id.mainFrmSelectedMenuChoice);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		Save saver = new Save();
		if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
			if (this.uri == null)
				this.uri = Uri.parse(readFromDevice("uriNewImage").toString());

			removeFromDevice("uriNewImage");
			try {
				String imageurl = getRealPathFromURI(uri);
				Bitmap bitmapImage = BitmapFactory.decodeFile(imageurl);
				saver.SaveImage(MainActivity.this, bitmapImage);
				(new AddPicture(bitmapImage)).execute();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		if (requestCode == REQUEST_IMAGE_GALLERY && resultCode == RESULT_OK) {
			Uri selectedImage = data.getData();
			String picturePath = getRealPathFromURI(selectedImage);
			Bitmap bitmap = BitmapFactory.decodeFile(picturePath);

			(new AddPicture(bitmap)).execute();
		}

		if (requestCode == PAGE_REFRESH && resultCode == RESULT_OK)
			activityOpen(MainActivity.class, true);
	}

	public boolean onOptionsItemSelected(MenuItem menuItem) {
		if (getMainDrlMenu().isDrawerOpen(GravityCompat.START))
			getMainDrlMenu().closeDrawers();
		else
			getMainDrlMenu().openDrawer(GravityCompat.START);

		return (super.onOptionsItemSelected(menuItem));
	}

	public void travelsBtnAddTravel_onClick(View view) {
		HashMap<String, Object> params = new HashMap<String, Object>();
		params.put("Id", null);
		activityOpenForResult(PlanningTravelActivity.class, params, PAGE_REFRESH);
	}

	public void setId(int id) {
		this.selectedTravel = id;
	}

	public void setUri(Uri uri) {
		this.uri = uri;
	}

	private class AddPicture extends ParallelTask {
		private Bitmap image;
		private byte[] data;

		protected void beforeExecution() {
			// durante l'attesa disattivo la rotazione
			rotationLock();
			// espongo il box di attesa
			waitingShow(getString(R.string.app_attendere), getString(R.string.travel_aggiungi_foto));
		}

		public AddPicture(Bitmap image) {
			this.image = image;
			this.data = null;
		}

		protected Object execution() {
			try {
				// richiedo al server l'inserimento
				HashMap<String, Object> cmd = new HashMap<String, Object>();
				cmd.put("Extension", getFileExtension(image));
				if (selectedTravel == null)
					cmd.put("TravelId", readFromDevice("travelId").toString());
				else
					cmd.put("TravelId", selectedTravel.toString());

				if (image != null) {
					ByteArrayOutputStream bos = new ByteArrayOutputStream();
					image.compress(CompressFormat.JPEG, 85, bos);
					data = bos.toByteArray();
					String encodedImage = Base64.encodeToString(data, Base64.DEFAULT);
					cmd.put("Image", encodedImage);
				}

				removeFromDevice("travelId");

				JSONObject model = ApiContext.travelBook().post("travels/addPicture", cmd, MainActivity.this);

				return (model);
			} catch (Exception e) {
				return (null);
			}
		}

		protected void progress(Object params) {
		}

		protected void afterExecution(Object output) {
			try {
				// riattivo la rotazione
				rotationUnlock();

				// elaboro la risposta del server
				JSONObject model = (JSONObject) output;
				if (model == null) {
					waitingHide();
					alert(getString(R.string.app_errore), getString(R.string.app_problema_connessione_foto));
				} else {
					if (model.getBoolean("Success") == true) {
						setResult(RESULT_OK);
						trimCache(MainActivity.this);
						activityOpen(MainActivity.class, true);
					} else {
						waitingHide();
						toast(model.getString("ErrorDescription"));
					}
				}
			} catch (JSONException e) {
				waitingHide();
				alert(getString(R.string.app_errore), getString(R.string.app_problema_comunicazione_foto));
			}
		}

		protected void onError() {
			// riattivo la rotazione
			rotationUnlock();
			waitingHide();
		}
	}

	private class Logout extends ParallelTask {
		protected void beforeExecution() {
			// durante l'attesa disattivo la rotazione
			rotationLock();

			// espongo il box di attesa
			waitingShow(getString(R.string.app_attendere), getString(R.string.main_logout_in_corso));
		}

		protected Object execution() {
			try {
				// richiedo il logout al server
				HashMap<String, Object> cmd = new HashMap<String, Object>();
				JSONObject model = ApiContext.travelBook().get("session/logout", cmd);

				return (model);
			} catch (Exception e) {
				return (null);
			}
		}

		protected void progress(Object params) {
		}

		protected void afterExecution(Object output) {
			removeFromDevice("email");
			removeFromDevice("password");

			activityOpen(AccessActivity.class, true);
		}

		protected void onError() {
			rotationUnlock();
			waitingHide();
		}
	}
}
