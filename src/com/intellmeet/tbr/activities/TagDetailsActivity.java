package com.intellmeet.tbr.activities;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Base64;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.intellmeet.tbr.ParallelTask;
import com.intellmeet.tbr.http.ApiContext;
import com.intellmeet.tbr.MasterActivity;
import com.intellmeet.tbr.R;
import com.intellmeet.tbr.Save;

public class TagDetailsActivity extends MasterActivity {
	private final int IMG_MAX_DIM_SIZE = 500;
	private final int VIEW_STATE_READ_TAG = 0;
	private final int VIEW_STATE_EDIT_TAG = 1;
	private final int REQUEST_IMAGE_CAPTURE_TAG = 3;
	private final int REQUEST_IMAGE_GALLERY_TAG = 4;

	private boolean isFinalized;
	private Uri uri;

	private ProgressBar getReadTagPrbWaiting() {
		return ((ProgressBar) findViewById(R.id.readTagPrbWaiting));
	}

	private TextView getReadTagLblTitle() {
		return ((TextView) findViewById(R.id.readTagLblTitle));
	}

	private ImageView getReadTagImage() {
		return ((ImageView) findViewById(R.id.readTagImage));
	}

	private TextView getReadTagLblDate() {
		return ((TextView) findViewById(R.id.readTagLblDate));
	}

	private TextView getReadTagLblArea() {
		return ((TextView) findViewById(R.id.readTagLblArea));
	}

	private TextView getReadTagLblPlace() {
		return ((TextView) findViewById(R.id.readTagLblPlace));
	}

	private TextView getReadTagLblNote() {
		return ((TextView) findViewById(R.id.readTagLblNote));
	}

	private TextView getEditTagLblTitle() {
		return ((TextView) findViewById(R.id.editTagLblTitle));
	}

	private TextView getEditTagLblPlace() {
		return ((TextView) findViewById(R.id.editTagLblPlace));
	}

	private EditText getEditTagTxtNote() {
		return ((EditText) findViewById(R.id.editTagNote));
	}

	private LinearLayout getEditTagTitleLayout() {
		return ((LinearLayout) findViewById(R.id.editTitleLayout));
	}

	protected void onCreate(Bundle bundle) {
		super.onCreate(bundle);

		isFinalized = false;

		setWaitingTheme(ProgressDialog.THEME_HOLO_DARK);
		setAlertTheme(AlertDialog.THEME_HOLO_DARK);
		setDatePickerTheme(DatePickerDialog.THEME_HOLO_DARK);
		setTimePickerTheme(TimePickerDialog.THEME_HOLO_DARK);

		int state = (Integer) getActivityParam("State");

		switch (state) {
		case 0:
			readTagPageShow();
			break;

		case 1:
			editTagPageShow();
			break;
		}
	}

	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		if (isFinalized && (Integer) getActivityParam("State") == 0) {
			menu.getItem(1).setEnabled(true);
			menu.getItem(2).setEnabled(true);
		}
		return true;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		if ((Integer) getActivityParam("State") == 0) {
			MenuInflater inflater = getMenuInflater();
			inflater.inflate(R.menu.details_menu, menu);

			menu.getItem(1).setEnabled(false);
			menu.getItem(2).setEnabled(false);
		} else {
			MenuInflater inflater = getMenuInflater();
			inflater.inflate(R.menu.edit_menu, menu);
		}
		return (true);
	}

	private void editTagPageShow() {
		setViewState(VIEW_STATE_EDIT_TAG);
		// espongo la page
		setContentView(R.layout.edit_tag_page);

		// settaggio della action bar
		ColorDrawable colorDrawable = new ColorDrawable(Color.parseColor("#06A1A4"));
		getActionBar().setBackgroundDrawable(colorDrawable);
		getActionBar().setIcon(R.drawable.ic_left_arrow_w);
		getActionBar().setTitle(getString(R.string.tag_indietro));
		getActionBar().setHomeButtonEnabled(true);

		// Posiziono la EditText
		setTopFromComponent(getEditTagTitleLayout(), getEditTagTxtNote(), 10);

		getEditTagLblTitle().setText(getActivityParam("Title").toString() + ": ");
		getEditTagLblPlace().setText(getActivityParam("Place").toString());
		getEditTagTxtNote().setText(getActivityParam("Note").toString());
	}

	private void readTagPageShow() {
		setViewState(VIEW_STATE_READ_TAG);
		// espongo la page
		setContentView(R.layout.read_tag_page);

		// settaggio della action bar
		ColorDrawable colorDrawable = new ColorDrawable(Color.parseColor("#06A1A4"));
		getActionBar().setBackgroundDrawable(colorDrawable);
		getActionBar().setIcon(R.drawable.ic_left_arrow_w);
		getActionBar().setTitle(getString(R.string.travel_indietro));
		getActionBar().setHomeButtonEnabled(true);

		// posizionamento dei componenti
		setTop(getReadTagPrbWaiting(), 45);
		setTop(getReadTagLblTitle(), 10);
		setTopFromComponent(getReadTagLblTitle(), getReadTagImage(), 10);
		setTopFromComponent(getReadTagImage(), getReadTagLblDate(), 10);
		bottomAlignment(getReadTagLblDate(), getReadTagLblPlace());
		bottomAlignment(getReadTagLblDate(), getReadTagLblArea());
		setTopFromComponent(getReadTagLblDate(), getReadTagLblNote(), 10);

		(new GetTag()).execute();
	}

	public boolean onOptionsItemSelected(MenuItem menuItem) {
		boolean result = super.onOptionsItemSelected(menuItem);

		if (menuItem.getItemId() == R.id._detailsMenuPictureButton) {
			List<String> listItems = new ArrayList<String>();
			listItems.add("Fotocamera");
			listItems.add("Galleria");

			final CharSequence[] charSequenceItems = listItems.toArray(new CharSequence[listItems.size()]);

			AlertDialog.Builder builder = new AlertDialog.Builder(TagDetailsActivity.this, 2);
			builder.setTitle("Inserimento Foto").setItems(charSequenceItems, new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int position) {
					switch (position) {
					case 0:
						ContentValues values = new ContentValues();
						values.put(MediaStore.Images.Media.TITLE, "New Picture");
						values.put(MediaStore.Images.Media.DESCRIPTION, "From your Camera");
						Uri imageUri = getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
						Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
						takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
						if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
							saveToDevice("uriNewImage", imageUri.toString());
							setUri(imageUri);
							startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE_TAG);
						}
						break;

					case 1:
						Intent galleryIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
						if (galleryIntent.resolveActivity(getPackageManager()) != null) {
							startActivityForResult(galleryIntent, REQUEST_IMAGE_GALLERY_TAG);
						}
						break;
					}
				}
			});

			builder.create();
			builder.show();
			return (result);
		}

		if (menuItem.getItemId() == R.id._editMenuEditButton) {
			HashMap<String, Object> params = new HashMap<String, Object>();
			params.put("Id", getActivityParam("Id"));
			params.put("State", 1);
			params.put("Title", getReadTagLblTitle().getText());
			params.put("Place", getReadTagLblPlace().getText());
			params.put("Note", getReadTagLblNote().getText());

			activityOpen(TagDetailsActivity.class, true, params);
			return (result);
		}

		if (menuItem.getItemId() == R.id._editMenuSeeTagButton) {
			HashMap<String, Object> params = new HashMap<String, Object>();
			params.put("TagID", getActivityParam("Id"));
			params.put("TravelID", null);

			activityOpen(MapActivity.class, true, params);
			return (result);
		}

		if (menuItem.getItemId() == R.id._editMenuSaveButton) {
			(new EditTag()).execute();
		}
		finish();
		return (result);
	}

	public void setUri(Uri imageUri) {
		this.uri = imageUri;
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		Save saver = new Save();
		if (requestCode == REQUEST_IMAGE_CAPTURE_TAG && resultCode == RESULT_OK) {
			if (this.uri == null)
				this.uri = Uri.parse(readFromDevice("uriNewImage").toString());

			removeFromDevice("uriNewImage");
			try {
				String imageurl = getRealPathFromURI(uri);
				Bitmap bitmapImage = BitmapFactory.decodeFile(imageurl);
				saver.SaveImage(TagDetailsActivity.this, bitmapImage);
				(new AddTagPicture(bitmapImage)).execute();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		if (requestCode == REQUEST_IMAGE_GALLERY_TAG && resultCode == RESULT_OK) {
			Uri selectedImage = data.getData();
			String picturePath = getRealPathFromURI(selectedImage);
			Bitmap bitmap = BitmapFactory.decodeFile(picturePath);

			(new AddTagPicture(bitmap)).execute();
		}
	}

	private class AddTagPicture extends ParallelTask {
		private Bitmap image;
		private byte[] data;

		protected void beforeExecution() {
			// durante l'attesa disattivo la rotazione
			rotationLock();
			// espongo il box di attesa
			waitingShow(getString(R.string.app_attendere), getString(R.string.tag_aggiungi_foto));
		}

		public AddTagPicture(Bitmap image) {
			this.image = image;
			this.data = null;
		}

		protected Object execution() {
			try {
				// richiedo al server l'inserimento
				HashMap<String, Object> cmd = new HashMap<String, Object>();
				cmd.put("Extension", getFileExtension(image));
				cmd.put("TagID", getActivityParam("Id").toString());

				if (image != null) {
					ByteArrayOutputStream bos = new ByteArrayOutputStream();
					image.compress(Bitmap.CompressFormat.JPEG, 85, bos);
					data = bos.toByteArray();
					String encodedImage = Base64.encodeToString(data, Base64.NO_WRAP);
					cmd.put("Image", encodedImage);
				}

				JSONObject model = ApiContext.travelBook().post("tags/addPicture", cmd, TagDetailsActivity.this);

				return (model);
			} catch (Exception e) {
				return (null);
			}
		}

		protected void progress(Object params) {
		}

		protected void afterExecution(Object output) {
			try {
				// riattivo la rotazione
				rotationUnlock();

				// elaboro la risposta del server
				JSONObject model = (JSONObject) output;
				if (model == null) {
					waitingHide();
					alert(getString(R.string.app_errore), getString(R.string.app_problema_connessione_foto));
				} else {
					if (model.getBoolean("Success") == true) {
						setResult(RESULT_OK);
						HashMap<String, Object> params = new HashMap<String, Object>();
						params.put("Id", getActivityParam("Id"));
						params.put("TravelId", getActivityParam("TravelId"));
						params.put("State", 0);
						trimCache(TagDetailsActivity.this);
						activityOpen(TagDetailsActivity.class, true, params);
					} else {
						waitingHide();
						toast(model.getString("ErrorDescription"));
					}
				}
			} catch (JSONException e) {
				waitingHide();
				alert(getString(R.string.app_errore), getString(R.string.app_problema_comunicazione_foto));
			}
		}

		protected void onError() {
			// riattivo la rotazione
			rotationUnlock();
			waitingHide();
		}
	}

	private class GetTag extends ParallelTask {

		protected void beforeExecution() {
			componentShow(getReadTagPrbWaiting());
		}

		protected Object execution() {
			try {
				// richiedo al server il dettaglio del tag
				HashMap<String, Object> cmd = new HashMap<String, Object>();
				cmd.put("TagID", getActivityParam("Id"));
				JSONObject model = ApiContext.travelBook().get("tags/getTag", cmd, TagDetailsActivity.this);

				// scarico l'immagine da visualizzare
				if (model.getJSONObject("Data").getString("ImageUrl").compareTo("null") != 0 && getUrlImage(model.getJSONObject("Data").getString("ImageUrl"), IMG_MAX_DIM_SIZE) == null)
					downloadUrlImage(model.getJSONObject("Data").getString("ImageUrl"), TagDetailsActivity.this);

				// salvo il model su disco
				saveToDevice("tag" + getActivityParam("Id"), model.toString());

				return (model);
			} catch (Exception exception1) {
				try {
					// prelevo il risultato dell'ultima chiamata andata a buon
					// fine
					JSONObject oldModel = null;
					String strOldModel = (String) readFromDevice("tag" + getActivityParam("Id"));
					if (strOldModel != null)
						oldModel = new JSONObject(strOldModel);

					return (oldModel);
				} catch (Exception exception2) {
					return (null);
				}
			}
		}

		protected void progress(Object params) {
		}

		protected void afterExecution(Object output) {
			try {
				componentHide(getReadTagPrbWaiting());

				JSONObject model = (JSONObject) output;

				// probabile problema di connessione
				if (model == null)
					alert(getString(R.string.app_errore), getString(R.string.app_problema_connessione));
				else {
					// immagine da visualizzare
					Bitmap image = getUrlImage(model.getJSONObject("Data").getString("ImageUrl"), IMG_MAX_DIM_SIZE);
					if (image == null)
						image = getResImage(R.drawable.no_photo_img, IMG_MAX_DIM_SIZE);

					// data binding
					getReadTagLblTitle().setText(model.getJSONObject("Data").getString("Name"));
					getReadTagImage().setImageBitmap(image);
					if (model.getJSONObject("Data").getString("Address").length() > 21) {
						getReadTagLblPlace().setText(model.getJSONObject("Data").getString("Address"));
						getReadTagLblArea().setText("");
					} else {
						getReadTagLblPlace().setText(model.getJSONObject("Data").getString("Address"));
						getReadTagLblArea().setText(model.getJSONObject("Data").getString("Area"));
					}
					getReadTagLblDate().setText(dateTimeFormat(jsonDateParse(model.getJSONObject("Data").getString("Date"))));
					getReadTagLblNote().setText(model.getJSONObject("Data").getString("Note"));

					if (getReadTagLblNote().getText().equals("null"))
						getReadTagLblNote().setText("");

					isFinalized = true;
					// aggiornamento dei pulsanti sulla action bar
					invalidateOptionsMenu();
				}
			} catch (JSONException e) {
				alert(getString(R.string.app_errore), getString(R.string.app_problema_comunicazione));
			}
		}

		protected void onError() {
		}
	}

	private class EditTag extends ParallelTask {

		protected void beforeExecution() {
			// durante l'attesa disattivo la rotazione
			rotationLock();
		}

		protected Object execution() {
			try {
				HashMap<String, Object> cmd = new HashMap<String, Object>();
				cmd.put("TagID", getActivityParam("Id").toString());
				cmd.put("Note", getEditTagTxtNote().getText());
				JSONObject model = ApiContext.travelBook().post("tags/editTag", cmd, TagDetailsActivity.this);

				return (model);
			} catch (Exception exception1) {
				return (null);
			}
		}

		protected void progress(Object params) {
		}

		protected void afterExecution(Object output) {
			try {
				// riattivo la rotazione
				rotationUnlock();

				// elaboro la risposta del server
				JSONObject model = (JSONObject) output;
				if (model == null)
					alert(getString(R.string.app_errore), getString(R.string.app_problema_connessione));
				else {
					if (model.getBoolean("Success") == true) {
						finish();
					} else
						toast(model.getString("ErrorDescription"));
				}
			} catch (JSONException e) {
				alert(getString(R.string.app_errore), getString(R.string.app_problema_comunicazione));
			}
		}

		protected void onError() {
		}
	}
}