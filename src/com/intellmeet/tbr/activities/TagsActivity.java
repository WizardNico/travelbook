package com.intellmeet.tbr.activities;

import java.io.ByteArrayOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.util.Base64;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.AdapterView.OnItemClickListener;

import com.intellmeet.tbr.ParallelTask;
import com.intellmeet.tbr.activities.AccessActivity;
import com.intellmeet.tbr.http.ApiContext;
import com.intellmeet.tbr.adapters.MenuAdapter;
import com.intellmeet.tbr.adapters.dataitems.MenuAdapterDataItem;
import com.intellmeet.tbr.fragments.TagsFragment;
import com.intellmeet.tbr.GPSTracker;
import com.intellmeet.tbr.R;
import com.intellmeet.tbr.MasterActivity;
import com.intellmeet.tbr.Save;

public class TagsActivity extends MasterActivity {
	private final int REQUEST_IMAGE_CAPTURE = 1;
	private final int REQUEST_IMAGE_GALLERY = 2;
	private final int REQUEST_IMAGE_CAPTURE_TAG = 3;
	private final int REQUEST_IMAGE_GALLERY_TAG = 4;

	private GPSTracker gps;
	private Integer selectedTag;
	private Uri uri;

	private ListView getMainLswMenuChoices() {
		return ((ListView) findViewById(R.id.travelLswMenuChoices));
	}

	private DrawerLayout getMainDrlMenu() {
		return ((DrawerLayout) findViewById(R.id.travelDrlMenu));
	}

	protected void onCreate(Bundle bundle) {
		super.onCreate(bundle);

		setAlertTheme(AlertDialog.THEME_HOLO_DARK);
		setWaitingTheme(ProgressDialog.THEME_HOLO_DARK);

		// Create class object
		gps = new GPSTracker(TagsActivity.this);

		mainPageShow();
	}

	private void mainPageShow() {
		// espongo il layout
		setContentView(R.layout.travel_page);

		// settaggio della action bar
		ColorDrawable colorDrawable = new ColorDrawable(Color.parseColor("#06A1A4"));
		getActionBar().setBackgroundDrawable(colorDrawable);
		getActionBar().setIcon(R.drawable.ic_three_lines_w);
		getActionBar().setTitle(getString(R.string.main_menu));
		getActionBar().setHomeButtonEnabled(true);

		// creo il menu
		ArrayList<MenuAdapterDataItem> menuAdapterDataItems = new ArrayList<MenuAdapterDataItem>();
		menuAdapterDataItems.add(new MenuAdapterDataItem(getResImage(R.drawable.ic_action_social_public), getString(R.string.main_viaggi)));
		menuAdapterDataItems.add(new MenuAdapterDataItem(getResImage(R.drawable.ic_action_maps_map), getString(R.string.main_crea_viaggio)));
		menuAdapterDataItems.add(new MenuAdapterDataItem(getResImage(R.drawable.ic_exit_arrow_w), getString(R.string.main_logout)));
		MenuAdapter menuAdapter = new MenuAdapter(menuAdapterDataItems, this);
		getMainLswMenuChoices().setAdapter(menuAdapter);

		// attivo la selezione sul menu
		getMainLswMenuChoices().setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
				if (position == 0)
					openHomeActivity(MainActivity.class);

				if (position == 1) {
					HashMap<String, Object> params = new HashMap<String, Object>();
					params.put("Id", null);
					activityOpen(PlanningTravelActivity.class, true, params);
				}

				if (position == 2)
					confirmAlert(getString(R.string.app_attenzione), getString(R.string.main_conferma_logout), new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int which) {
							(new Logout()).execute();
						}
					}, null);

				if (position != 2)
					getMainDrlMenu().closeDrawers();
			}
		});

		// default per la voce di menu selezionata
		if (isThereFragment(R.id.travelFrmSelectedMenuChoice) == false)
			fragmentOpen(TagsFragment.class, R.id.travelFrmSelectedMenuChoice);
	}

	public boolean onOptionsItemSelected(MenuItem menuItem) {
		boolean result = super.onOptionsItemSelected(menuItem);

		switch (menuItem.getItemId()) {
		case R.id._editMenuAddTravelPhoto:
			List<String> listItems = new ArrayList<String>();
			listItems.add("Fotocamera");
			listItems.add("Galleria");

			final CharSequence[] charSequenceItems = listItems.toArray(new CharSequence[listItems.size()]);

			AlertDialog.Builder builder = new AlertDialog.Builder(TagsActivity.this);
			builder.setTitle("Inserimento Foto").setItems(charSequenceItems, new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int position) {
					switch (position) {
					case 0:
						ContentValues values = new ContentValues();
						values.put(MediaStore.Images.Media.TITLE, "New Picture");
						values.put(MediaStore.Images.Media.DESCRIPTION, "From your Camera");
						Uri imageUri = getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
						Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
						takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
						if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
							saveToDevice("uriNewImage", imageUri.toString());
							setUri(imageUri);
							startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
						}
						break;

					case 1:
						Intent galleryIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
						if (galleryIntent.resolveActivity(getPackageManager()) != null) {
							startActivityForResult(galleryIntent, REQUEST_IMAGE_GALLERY);
						}
						break;
					}
				}
			});

			builder.create();
			builder.show();
			break;

		case R.id._editMenuAddTag:
			// Check if GPS enabled
			if (!gps.canGetLocation()) {
				// Can't get location.
				// GPS or network is not enabled.
				// Ask user to enable GPS/network in settings.
				gps.showSettingsAlert();
			} else {
				(new CheckIn()).execute();
			}
			break;

		case R.id._editMenuTagCollection:
			HashMap<String, Object> params = new HashMap<String, Object>();
			params.put("TravelID", getActivityParam("Id"));
			params.put("TagID", null);
			activityOpen(MapActivity.class, false, params);
			break;

		default:
			if (getMainDrlMenu().isDrawerOpen(GravityCompat.START))
				getMainDrlMenu().closeDrawers();
			else
				getMainDrlMenu().openDrawer(GravityCompat.START);
			break;
		}
		return (result);
	}

	public void setId(int id) {
		this.selectedTag = id;
	}

	public void setUri(Uri imageUri) {
		this.uri = imageUri;
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		Save saver = new Save();
		if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
			if (this.uri == null)
				this.uri = Uri.parse(readFromDevice("uriNewImage").toString());

			removeFromDevice("uriNewImage");
			try {
				String imageurl = getRealPathFromURI(uri);
				Bitmap bitmapImage = BitmapFactory.decodeFile(imageurl);
				saver.SaveImage(TagsActivity.this, bitmapImage);
				(new AddTravelPicture(bitmapImage)).execute();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		if (requestCode == REQUEST_IMAGE_GALLERY && resultCode == RESULT_OK) {
			Uri selectedImage = data.getData();
			String picturePath = getRealPathFromURI(selectedImage);
			Bitmap bitmap = BitmapFactory.decodeFile(picturePath);

			(new AddTravelPicture(bitmap)).execute();
		}

		if (requestCode == REQUEST_IMAGE_CAPTURE_TAG && resultCode == RESULT_OK) {
			if (this.uri == null)
				this.uri = Uri.parse(readFromDevice("uriNewImage").toString());

			removeFromDevice("uriNewImage");
			try {
				String imageurl = getRealPathFromURI(uri);
				Bitmap bitmapImage = BitmapFactory.decodeFile(imageurl);
				saver.SaveImage(TagsActivity.this, bitmapImage);
				(new AddTagPicture(bitmapImage)).execute();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		if (requestCode == REQUEST_IMAGE_GALLERY_TAG && resultCode == RESULT_OK) {
			Uri selectedImage = data.getData();
			String picturePath = getRealPathFromURI(selectedImage);
			Bitmap bitmap = BitmapFactory.decodeFile(picturePath);

			(new AddTagPicture(bitmap)).execute();
		}
	}

	private class AddTagPicture extends ParallelTask {
		private Bitmap image;
		private byte[] data;

		protected void beforeExecution() {
			// durante l'attesa disattivo la rotazione
			rotationLock();
			// espongo il box di attesa
			waitingShow(getString(R.string.app_attendere), getString(R.string.tag_aggiungi_foto));
		}

		public AddTagPicture(Bitmap image) {
			this.image = image;
			this.data = null;
		}

		protected Object execution() {
			try {
				// richiedo al server l'inserimento
				HashMap<String, Object> cmd = new HashMap<String, Object>();
				cmd.put("Extension", getFileExtension(image));
				if (selectedTag == null)
					cmd.put("TagID", readFromDevice("tagId").toString());
				else
					cmd.put("TagID", selectedTag.toString());

				if (image != null) {
					ByteArrayOutputStream bos = new ByteArrayOutputStream();
					image.compress(Bitmap.CompressFormat.JPEG, 85, bos);
					data = bos.toByteArray();
					String encodedImage = Base64.encodeToString(data, Base64.NO_WRAP);
					cmd.put("Image", encodedImage);
				}

				removeFromDevice("tagId");

				JSONObject model = ApiContext.travelBook().post("tags/addPicture", cmd, TagsActivity.this);

				return (model);
			} catch (Exception e) {
				return (null);
			}
		}

		protected void progress(Object params) {
		}

		protected void afterExecution(Object output) {
			try {
				// riattivo la rotazione
				rotationUnlock();

				// elaboro la risposta del server
				JSONObject model = (JSONObject) output;
				if (model == null) {
					waitingHide();
					alert(getString(R.string.app_errore), getString(R.string.app_problema_connessione_foto));
				} else {
					if (model.getBoolean("Success") == true) {
						setResult(RESULT_OK);
						HashMap<String, Object> params = new HashMap<String, Object>();
						params.put("Id", getActivityParam("Id"));
						trimCache(TagsActivity.this);
						activityOpen(TagsActivity.class, true, params);
					} else
						toast(model.getString("ErrorDescription"));
				}
			} catch (JSONException e) {
				waitingHide();
				alert(getString(R.string.app_errore), getString(R.string.app_problema_comunicazione_foto));
			}
		}

		protected void onError() {
			// riattivo la rotazione
			rotationUnlock();
			waitingHide();
		}
	}

	private class CheckIn extends ParallelTask {
		protected void beforeExecution() {
			// durante l'attesa disattivo la rotazione
			rotationLock();
			// espongo il box di attesa
			waitingShow(getString(R.string.app_attendere), getString(R.string.tag_cerca_posizione));
		}

		protected Object execution() {
			try {
				// salvo le coordinate sul server
				HashMap<String, Object> cmd = new HashMap<String, Object>();

				Geocoder gcd = new Geocoder(TagsActivity.this, Locale.getDefault());
				double latitude = gps.getLatitude();
				double longitude = gps.getLongitude();

				Date date = new Date(gps.getTime());
				SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm");
				String dateTime = sdf.format(date);

				List<Address> addresses = gcd.getFromLocation(latitude, longitude, 1);
				Address address = addresses.get(0);

				if (address != null) {
					cmd.put("Name", address.getLocality());
					if (address.getAdminArea() != null)
						cmd.put("Area", address.getAdminArea());
					else
						cmd.put("Area", address.getSubAdminArea());
					cmd.put("Address", address.getAddressLine(0));
					cmd.put("Date", dateTime.toString());
				} else {
					cmd.put("Name", "Registrazione");
					cmd.put("Area", "");
					cmd.put("Address", "");
					cmd.put("Date", "");
				}

				cmd.put("Latitude", latitude);
				cmd.put("Longitude", longitude);
				cmd.put("TravelID", getActivityParam("Id").toString());

				JSONObject model = ApiContext.travelBook().post("tags/addTag", cmd);

				return (model);
			} catch (Exception e) {
				return (null);
			}
		}

		protected void progress(Object params) {
		}

		protected void afterExecution(Object output) {
			try {
				rotationUnlock();
				waitingHide();
				// elaboro la risposta del server
				JSONObject model = (JSONObject) output;
				if (model == null)
					alert(getString(R.string.app_errore), getString(R.string.app_problema_segnale));
				else {
					if (model.getBoolean("Success") == true) {
						HashMap<String, Object> params = new HashMap<String, Object>();
						params.put("TagId", model.getLong("Data"));

						fragmentOpen(TagsFragment.class, R.id.travelFrmSelectedMenuChoice);
					} else
						toast(model.getString("ErrorDescription"));
				}
			} catch (JSONException e) {
				alert(getString(R.string.app_errore), getString(R.string.app_problema_comunicazione));
			}
		}

		protected void onError() {
			rotationUnlock();
			waitingHide();
		}
	}

	private class Logout extends ParallelTask {
		protected void beforeExecution() {
			// durante l'attesa disattivo la rotazione
			rotationLock();

			// espongo il box di attesa
			waitingShow(getString(R.string.app_attendere), getString(R.string.main_logout_in_corso));
		}

		protected Object execution() {
			try {
				// richiedo il logout al server
				HashMap<String, Object> cmd = new HashMap<String, Object>();
				JSONObject model = ApiContext.travelBook().get("session/logout", cmd);

				return (model);
			} catch (Exception e) {
				return (null);
			}
		}

		protected void progress(Object params) {
		}

		protected void afterExecution(Object output) {
			removeFromDevice("email");
			removeFromDevice("password");

			activityOpen(AccessActivity.class, true);
		}

		protected void onError() {
			rotationUnlock();
			waitingHide();
		}
	}

	private class AddTravelPicture extends ParallelTask {
		private Bitmap image;
		private byte[] data;

		protected void beforeExecution() {
			// durante l'attesa disattivo la rotazione
			rotationLock();
			// espongo il box di attesa
			waitingShow(getString(R.string.app_attendere), getString(R.string.travel_aggiungi_foto));
		}

		public AddTravelPicture(Bitmap image) {
			this.image = image;
			this.data = null;
		}

		protected Object execution() {
			try {
				// richiedo al server l'inserimento
				HashMap<String, Object> cmd = new HashMap<String, Object>();
				cmd.put("TravelId", getActivityParam("Id").toString());
				cmd.put("Extension", getFileExtension(image));

				if (image != null) {
					ByteArrayOutputStream bos = new ByteArrayOutputStream();
					image.compress(Bitmap.CompressFormat.JPEG, 85, bos);
					data = bos.toByteArray();
					String encodedImage = Base64.encodeToString(data, Base64.NO_WRAP);
					cmd.put("Image", encodedImage);
				}

				JSONObject model = ApiContext.travelBook().post("travels/addPicture", cmd, TagsActivity.this);

				return (model);
			} catch (Exception e) {
				return (null);
			}
		}

		protected void progress(Object params) {
		}

		protected void afterExecution(Object output) {
			try {
				// riattivo la rotazione
				rotationUnlock();

				// elaboro la risposta del server
				JSONObject model = (JSONObject) output;
				if (model == null) {
					waitingHide();
					alert(getString(R.string.app_errore), getString(R.string.app_problema_connessione_foto));
				} else {
					if (model.getBoolean("Success") == true) {
						setResult(RESULT_OK);
						trimCache(TagsActivity.this);
						activityOpen(MainActivity.class, true);
					} else
						waitingHide();
					toast(model.getString("ErrorDescription"));
				}
			} catch (JSONException e) {
				alert(getString(R.string.app_errore), getString(R.string.app_problema_comunicazione_foto));
			}
		}

		protected void onError() {
			// riattivo la rotazione
			rotationUnlock();
			waitingHide();
		}
	}
}