package com.intellmeet.tbr;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.net.URL;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;

public abstract class ParallelTask extends AsyncTask<Void, Object, Object> {
	private final long SLEEP_MILLIS_BEFORE_EXECUTION = 400;

	protected abstract void beforeExecution();

	protected abstract Object execution();

	protected abstract void progress(Object params);

	protected abstract void afterExecution(Object output);

	protected abstract void onError();

	protected void onPreExecute() {
		beforeExecution();
	}

	protected Object doInBackground(Void... params) {
		try {
			Thread.sleep(SLEEP_MILLIS_BEFORE_EXECUTION);
		} catch (InterruptedException e) {
		}

		Object output = execution();

		return (output);
	}

	protected void onProgressUpdate(Object... params) {
		progress(params[0]);
	}

	protected void onPostExecute(Object param) {
		try {
			afterExecution(param);
		} catch (Exception e) {
			onError();
		}
	}

	protected void downloadUrlImage(String url, MasterActivity activity) {
		try {
			// scarico l'immagine
			Bitmap image = BitmapFactory.decodeStream((new URL(url)).openConnection().getInputStream());

			// salvo l'immagine su disco
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			image.compress(Bitmap.CompressFormat.JPEG, 40, baos);
			File file = new File(activity.getCacheDir().getAbsolutePath() + "/" + url.replace("/", "_"));
			file.createNewFile();
			FileOutputStream fos = new FileOutputStream(file);
			fos.write(baos.toByteArray());
			fos.close();
		} catch (Exception e) {
		}
	}
}