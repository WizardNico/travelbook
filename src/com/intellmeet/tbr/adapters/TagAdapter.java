package com.intellmeet.tbr.adapters;

import java.util.ArrayList;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.intellmeet.tbr.MasterActivity;
import com.intellmeet.tbr.MasterAdapter;
import com.intellmeet.tbr.R;
import com.intellmeet.tbr.adapters.dataitems.TagsAdapterDataItem;

public class TagAdapter extends MasterAdapter<TagsAdapterDataItem> {
	public TagAdapter(ArrayList<TagsAdapterDataItem> dataItems, MasterActivity activity) {
		super(dataItems, R.layout.tag_item, activity);
	}

	@Override
	protected void binding(TagsAdapterDataItem dataItem, View layoutItem) {
		((ImageView) layoutItem.findViewById(R.id.tagImage)).setImageBitmap(dataItem.getImage());
		((TextView) layoutItem.findViewById(R.id.tagTitle)).setText(dataItem.getTitle());
		((TextView) layoutItem.findViewById(R.id.tagDate)).setText(dataItem.getDate());
		((TextView) layoutItem.findViewById(R.id.tagNote)).setText(dataItem.getNote());
	}
}