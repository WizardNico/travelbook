package com.intellmeet.tbr.adapters;

import java.util.ArrayList;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.intellmeet.tbr.MasterActivity;
import com.intellmeet.tbr.MasterAdapter;
import com.intellmeet.tbr.R;
import com.intellmeet.tbr.adapters.dataitems.TravelsAdapterDataItem;

public class TravelAdapter extends MasterAdapter<TravelsAdapterDataItem> {
	public TravelAdapter(ArrayList<TravelsAdapterDataItem> dataItems, MasterActivity activity) {
		super(dataItems, R.layout.travel_item, activity);
	}

	protected void binding(TravelsAdapterDataItem dataItem, View layoutItem) {
		((ImageView) layoutItem.findViewById(R.id.travelImage)).setImageBitmap(dataItem.getImage());
		((TextView) layoutItem.findViewById(R.id.travelTitle)).setText(dataItem.getTitle());
		((TextView) layoutItem.findViewById(R.id.travelDepartureDate)).setText(dataItem.getDepartureDate());
		((TextView) layoutItem.findViewById(R.id.travelArrivalDate)).setText(dataItem.getArrivalDate());
	}
}