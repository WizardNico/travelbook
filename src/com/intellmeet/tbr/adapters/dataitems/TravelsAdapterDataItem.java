package com.intellmeet.tbr.adapters.dataitems;

import java.util.Date;

import android.graphics.Bitmap;

public class TravelsAdapterDataItem {
	private String title;
	private String departureDate;
	private String arrivalDate;
	private Bitmap image;

	public TravelsAdapterDataItem(String title, String departureDate, String arrivalDate, Bitmap image) {
		this.title = title;
		this.departureDate = departureDate;
		this.arrivalDate = arrivalDate;
		this.image = image;
	}
	
	public String getTitle() {
		return (title);
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDepartureDate() {
		return (departureDate);
	}

	public void setDepartureDate(Date departureDate) {
		this.departureDate = departureDate.toString();
	}

	public String getArrivalDate() {
		return (arrivalDate);
	}

	public void setArrivalDate(Date arrivalDate) {
		this.arrivalDate = arrivalDate.toString();
	}

	public Bitmap getImage() {
		return (image);
	}

	public void setImage(Bitmap image) {
		this.image = image;
	}
}