package com.intellmeet.tbr.adapters.dataitems;

import java.util.Date;

import android.graphics.Bitmap;

public class TagsAdapterDataItem {
	private String title;
	private String date;
	private Bitmap image;
	private String note;

	public TagsAdapterDataItem(String title, String date, Bitmap image, String note) {
		this.title = title;
		this.date = date;
		this.image = image;
		this.note = note;
	}

	public String getTitle() {
		return (title);
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDate() {
		return (date);
	}

	public void setDate(Date departureDate) {
		this.date = date.toString();
	}

	public Bitmap getImage() {
		return (image);
	}

	public void setImage(Bitmap image) {
		this.image = image;
	}

	public String getNote() {
		return (note);
	}

	public void setNote(String note) {
		this.note = note;
	}
}