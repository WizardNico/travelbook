package com.intellmeet.tbr.adapters.dataitems;

import android.graphics.Bitmap;

public class MenuAdapterDataItem {
	private Bitmap icon;
	private String label;

	public MenuAdapterDataItem(Bitmap icon, String label) {
		this.icon = icon;
		this.label = label;
	}

	public Bitmap getIcon() {
		return (icon);
	}

	public void setIcon(Bitmap icon) {
		this.icon = icon;
	}

	public String getLabel() {
		return (label);
	}

	public void setLabel(String label) {
		this.label = label;
	}
}