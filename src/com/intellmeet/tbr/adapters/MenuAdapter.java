package com.intellmeet.tbr.adapters;

import java.util.ArrayList;
import com.intellmeet.tbr.MasterActivity;
import com.intellmeet.tbr.MasterAdapter;
import com.intellmeet.tbr.R;
import com.intellmeet.tbr.adapters.dataitems.MenuAdapterDataItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class MenuAdapter extends MasterAdapter<MenuAdapterDataItem> {
	public MenuAdapter(ArrayList<MenuAdapterDataItem> dataItems, MasterActivity activity) {
		super(dataItems, R.layout.menu_item, activity);
	}

	protected void binding(MenuAdapterDataItem dataItem, View layoutItem) {
		((ImageView) layoutItem.findViewById(R.id.menuIcon)).setImageBitmap(dataItem.getIcon());
		((TextView) layoutItem.findViewById(R.id.menuLabel)).setText(dataItem.getLabel());
	}
}