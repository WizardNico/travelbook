package com.intellmeet.tbr;

public class Pagination {
	private int prevLastVisibleItem;
	private int pageSize;

	public Pagination(int pageSize) {
		this.pageSize = pageSize;
		prevLastVisibleItem = -1;
	}

	public boolean nextPage(int firstVisibleItem, int visibleItemCount, int totalItemCount, boolean load) {
		int lastVisibleItem = firstVisibleItem + visibleItemCount;

		boolean result = false;

		if (totalItemCount >= pageSize && lastVisibleItem == totalItemCount && prevLastVisibleItem != lastVisibleItem && load)
			result = true;

		prevLastVisibleItem = lastVisibleItem;

		return (result);
	}
}