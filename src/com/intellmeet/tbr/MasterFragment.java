package com.intellmeet.tbr;

import java.util.HashMap;
import android.app.DialogFragment;

public class MasterFragment extends DialogFragment {
	@SuppressWarnings("unchecked")
	protected Object getFragmentParam(String paramName) {
		return (((HashMap<String, Object>) getArguments().getSerializable("params")).get(paramName));
	}

	protected MasterActivity getLauncherActivity() {
		return ((MasterActivity) getActivity());
	}
}