package com.intellmeet.tbr.http;

import java.util.HashMap;
import org.json.JSONObject;
import com.intellmeet.tbr.MasterActivity;
import com.intellmeet.tbr.R;
import com.intellmeet.tbr.ApiClient;

public class TravelBookApiClient extends ApiClient {
	private static final String API_URL = "http://tbr.intellmeet.com/api";

	public TravelBookApiClient() {
		super(API_URL);
	}

	protected boolean check(MasterActivity activity) {
		try {
			// controllo lo stato della sessione
			HashMap<String, Object> cmd = new HashMap<String, Object>();
			JSONObject model = get("session/checkSession", cmd);

			if (model.getBoolean("Data") == true)
				return (true);

			// provo a rieseguire il login
			if (activity.readFromDevice("email") != null && activity.readFromDevice("password") != null) {
				cmd = new HashMap<String, Object>();
				cmd.put("Email", activity.readFromDevice("email"));
				cmd.put("Password", activity.readFromDevice("password"));
				model = get("session/login", cmd);

				if (model.getBoolean("Data") == true)
					return (true);
			}

			activity.removeFromDevice("email");
			activity.removeFromDevice("password");

			return (false);
		} catch (Exception e) {
			// i problemi di connessione vengono gestiti direttamente dalle
			// varie activity
			return (true);
		}
	}

	protected void checkFailed(MasterActivity activity) {
		activity.alert(activity.getString(R.string.app_errore), activity.getString(R.string.app_sessione_non_rinnovabile));
	}
}