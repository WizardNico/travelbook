package com.intellmeet.tbr.http;

import com.intellmeet.tbr.http.TravelBookApiClient;

public class ApiContext {
	private static TravelBookApiClient travelBookApiClient;

	static {
		travelBookApiClient = null;
	}

	public static TravelBookApiClient travelBook() {
		if (travelBookApiClient == null)
			travelBookApiClient = new TravelBookApiClient();

		return (travelBookApiClient);
	}
}