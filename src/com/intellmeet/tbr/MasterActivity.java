package com.intellmeet.tbr;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningServiceInfo;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.MediaStore.Images;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.view.Display;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import com.intellmeet.tbr.activities.MainActivity;

public class MasterActivity extends Activity {
	private final int SCREEN_SIZE_REFERENCE_VALUE = 384000;

	private int alertTheme;
	private int datePickerTheme;
	private int timePickerTheme;
	private int waitingTheme;
	private ProgressDialog waitingBox;

	public MasterActivity() {
		alertTheme = 0;
		datePickerTheme = 0;
		timePickerTheme = 0;
		waitingTheme = 0;
		waitingBox = null;
	}

	public static void trimCache(Context context) {
		File dir = context.getCacheDir();
		if (dir != null && dir.isDirectory()) {
			deleteDir(dir);
		}
	}

	private static boolean deleteDir(File dir) {
		if (dir != null && dir.isDirectory()) {
			String[] children = dir.list();
			for (int i = 0; i < children.length; i++) {
				boolean success = deleteDir(new File(dir, children[i]));
				if (!success) {
					return false;
				}
			}
		}

		// The directory is now empty so delete it
		return dir.delete();
	}

	protected String getFileExtension(Bitmap picture) {
		Uri tempUri = getImageUri(getApplicationContext(), picture);
		File finalFile = new File(getRealPathFromURI(tempUri));
		int dotposition = finalFile.getPath().lastIndexOf(".");
		return finalFile.getPath().substring(dotposition + 1, finalFile.getPath().length());
	}

	private Uri getImageUri(Context inContext, Bitmap inImage) {
		ByteArrayOutputStream bytes = new ByteArrayOutputStream();
		inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
		String path = Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
		return Uri.parse(path);
	}

	protected String getRealPathFromURI(Uri uri) {
		String[] filePathColumn = { MediaStore.Images.Media.DATA };

		Cursor cursor = getContentResolver().query(uri, filePathColumn, null, null, null);
		cursor.moveToFirst();

		int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
		String picturePath = cursor.getString(columnIndex);
		cursor.close();

		return picturePath;
	}

	protected void setAlertTheme(int alertTheme) {
		this.alertTheme = alertTheme;
	}

	protected void setDatePickerTheme(int datePickerTheme) {
		this.datePickerTheme = datePickerTheme;
	}

	protected void setTimePickerTheme(int timePickerTheme) {
		this.timePickerTheme = timePickerTheme;
	}

	protected void setWaitingTheme(int waitingTheme) {
		this.waitingTheme = waitingTheme;
	}

	protected void setViewState(int viewState) {
		getIntent().putExtra("viewState", viewState);
	}

	protected int getViewState() {
		return (getIntent().getIntExtra("viewState", 0));
	}

	public void setTop(View component, int distance) {
		RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) component.getLayoutParams();
		params.addRule(RelativeLayout.ALIGN_PARENT_TOP);
		params.topMargin = distance;
	}

	public void setBottom(View component, int distance) {
		RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) component.getLayoutParams();
		params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
		params.bottomMargin = distance;
	}

	public void setTopFromComponent(View referenceComponent, View toSpaceComponent, int distance) {
		RelativeLayout.LayoutParams toSpaceParams = (RelativeLayout.LayoutParams) toSpaceComponent.getLayoutParams();
		toSpaceParams.addRule(RelativeLayout.BELOW, referenceComponent.getId());
		toSpaceParams.topMargin = distance;
	}

	public void bottomAlignment(View referenceComponent, View toAlignComponent) {
		RelativeLayout.LayoutParams toAlignParams = (RelativeLayout.LayoutParams) toAlignComponent.getLayoutParams();
		toAlignParams.addRule(RelativeLayout.ALIGN_BOTTOM, referenceComponent.getId());
	}

	public boolean isLandscapeOrientation() {
		return (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE);
	}

	public void alert(String title, String message) {
		AlertDialog.Builder alertBuilder = new AlertDialog.Builder(this, alertTheme);
		alertBuilder.setTitle(title);
		alertBuilder.setMessage(message);
		AlertDialog alert = alertBuilder.create();
		alert.show();
	}

	public void confirmAlert(String title, String message, DialogInterface.OnClickListener onClickPositive, DialogInterface.OnClickListener onClickNegative) {
		AlertDialog.Builder alertBuilder = new AlertDialog.Builder(this, alertTheme);
		alertBuilder.setTitle(title);
		alertBuilder.setMessage(message);
		alertBuilder.setPositiveButton(android.R.string.yes, onClickPositive);
		alertBuilder.setNegativeButton(android.R.string.no, onClickNegative);
		AlertDialog alert = alertBuilder.create();
		alert.show();
	}

	public void waitingShow(String title, String message) {
		waitingBox = new ProgressDialog(this, waitingTheme);
		waitingBox.setTitle(title);
		waitingBox.setMessage(message);
		waitingBox.setCancelable(false);
		waitingBox.setProgressStyle(ProgressDialog.STYLE_SPINNER);
		waitingBox.show();
	}

	public void waitingHide() {
		waitingBox.hide();
	}

	public void underline(TextView textView) {
		SpannableString spannableString = new SpannableString(textView.getText());
		spannableString.setSpan(new UnderlineSpan(), 0, spannableString.length(), 0);
		textView.setText(spannableString);
	}

	public void setFont(TextView textView, String fontName) {
		Typeface typeFace = Typeface.createFromAsset(getAssets(), "fonts/" + fontName + ".ttf");
		textView.setTypeface(typeFace);
	}

	public void dateSelect(EditText editText) {
		// parsing della data
		Date date = new Date();
		try {
			String strDate = editText.getText().toString();
			DateFormat dateFormat = android.text.format.DateFormat.getDateFormat(getApplicationContext());
			date = dateFormat.parse(strDate);
		} catch (Exception e) {
		}

		// trasformo la data in un set di tre valori
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		int year = calendar.get(Calendar.YEAR);
		int month = calendar.get(Calendar.MONTH);
		int day = calendar.get(Calendar.DAY_OF_MONTH);

		// espongo il selettore con impostata la data che era gi�presente sul
		// campo di testo
		DatePickerDialog dialog = new DatePickerDialog(MasterActivity.this, datePickerTheme, new DateSetListener(editText), year, month, day);
		dialog.show();
	}

	public void timeSelect(EditText editText) {
		// parsing dell'ora
		TimeSpan time = new TimeSpan();
		try {
			String strTime = editText.getText().toString();
			time = new TimeSpan(strTime);
		} catch (Exception e) {
		}

		// espongo il selettore con impostata l'ora che era gi�presente sul
		// campo di testo
		TimePickerDialog dialog = new TimePickerDialog(MasterActivity.this, timePickerTheme, new TimeSetListener(editText), time.getHours(), time.getMinutes(), true);
		dialog.show();
	}

	public Date toDate(EditText editText) {
		try {
			DateFormat dateFormat = android.text.format.DateFormat.getDateFormat(getApplicationContext());
			Date date = dateFormat.parse(editText.getText().toString());

			return (date);
		} catch (Exception e) {
			return (null);
		}
	}

	public TimeSpan toTime(EditText editText) {
		try {
			TimeSpan time = new TimeSpan(editText.getText().toString());

			return (time);
		} catch (Exception e) {
			return (null);
		}
	}

	public void toast(String message) {
		Toast toast = Toast.makeText(this, message, Toast.LENGTH_LONG);
		toast.show();
	}

	public void activityOpen(Class<?> activity, boolean closeCurrentActivity, HashMap<String, Object> params) {
		// creo l'intent
		Intent intent = new Intent(this, activity);

		// passaggio dei parametri
		if (params != null && params.size() > 0)
			intent.putExtra("params", params);

		// lancio l'activity
		MasterActivity.this.startActivity(intent);

		// se richiesto chiudo l'activity corrente
		if (closeCurrentActivity == true)
			finish();
	}

	public void activityOpen(Class<?> activity, boolean closeCurrentActivity) {
		activityOpen(activity, closeCurrentActivity, null);
	}

	@SuppressWarnings("unchecked")
	public Object getActivityParam(String paramName) {
		return (((HashMap<String, Object>) getIntent().getSerializableExtra("params")).get(paramName));
	}

	public void saveToDevice(String label, Object data) {
		try {
			if (data == null)
				removeFromDevice(label);
			else {
				FileOutputStream fos = openFileOutput(label + ".dat", Context.MODE_PRIVATE);
				ObjectOutputStream oos = new ObjectOutputStream(fos);
				oos.writeObject(data);
				oos.close();
			}
		} catch (Exception e) {
		}
	}

	public void dateSelectFromSelectedDate(EditText editText, Date date) {
		// parsing della data
		try {
			String strDate = editText.getText().toString();
			DateFormat dateFormat = android.text.format.DateFormat.getDateFormat(getApplicationContext());
			date = dateFormat.parse(strDate);
		} catch (Exception e) {
		}

		// trasformo la data in un set di tre valori
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		int year = calendar.get(Calendar.YEAR);
		int month = calendar.get(Calendar.MONTH);
		int day = calendar.get(Calendar.DAY_OF_MONTH);

		// espongo il selettore con impostata la data che era gi�presente sul
		// campo di testo
		DatePickerDialog dialog = new DatePickerDialog(MasterActivity.this, datePickerTheme, new DateSetListener(editText), year, month, day + 1);
		dialog.show();
	}

	public void openHomeActivity(Class<?> activity) {
		Intent intent = new Intent(this, activity);
		intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
		MasterActivity.this.startActivity(intent);
		finish();
	}

	public Object readFromDevice(String label) {
		try {
			FileInputStream fis = openFileInput(label + ".dat");
			ObjectInputStream ois = new ObjectInputStream(fis);
			Object data = ois.readObject();
			ois.close();

			return (data);
		} catch (Exception e) {
			return (null);
		}
	}

	public void removeFromDevice(String label) {
		try {
			File file = new File(getFilesDir().getAbsolutePath(), label + ".dat");
			file.delete();
		} catch (Exception e) {
		}
	}

	public int getScreenSizeVal() {
		Display display = getWindowManager().getDefaultDisplay();
		Point point = new Point();
		display.getSize(point);
		int result = (int) ((((double) (point.x * point.y)) / ((double) SCREEN_SIZE_REFERENCE_VALUE)) + 0.5);

		return (result);
	}

	public Bitmap getResImage(int resImageId) {
		return (BitmapFactory.decodeResource(getResources(), resImageId));
	}

	public Bitmap getResImage(int resImageId, int maxDimSize) {
		return (resize(getResImage(resImageId), maxDimSize));
	}

	protected void fragmentOpen(Class<?> fragment, int frameLayoutId) {
		try {
			FragmentTransaction transaction = getFragmentManager().beginTransaction();
			transaction.replace(frameLayoutId, (MasterFragment) fragment.newInstance(), "" + frameLayoutId);
			transaction.commit();
		} catch (Exception e) {
		}
	}

	protected boolean isThereFragment(int frameLayoutId) {
		if (getFragmentManager().findFragmentByTag("" + frameLayoutId) == null)
			return (false);
		else
			return (true);
	}

	public Bitmap getUrlImage(String url, int maxDimSize) {
		try {
			// prelevo l'immagine da file
			Bitmap image = BitmapFactory.decodeStream(new FileInputStream(new File(this.getCacheDir().getAbsolutePath() + "/" + url.replace("/", "_"))));
			image = resize(image, maxDimSize);

			return (image);
		} catch (Exception e) {
			return (null);
		}
	}

	public void rotationLock() {
		if (isLandscapeOrientation() == true)
			setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
		else
			setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
	}

	public void rotationUnlock() {
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR);
	}

	public String dateTimeFormat(Date date) {
		// oggetti utili alla formattazione della data
		DateFormat dateFormat = android.text.format.DateFormat.getDateFormat(getApplicationContext());
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);

		// formattazione
		String hours = calendar.get(Calendar.HOUR_OF_DAY) + "";
		String minutes = calendar.get(Calendar.MINUTE) + "";
		if (hours.length() == 1)
			hours = "0" + hours;

		if (minutes.length() == 1)
			minutes = "0" + minutes;

		String result = dateFormat.format(date);
		result += " " + hours + ":" + minutes;

		return (result);
	}

	public Date jsonDateParse(String jsonDate) {
		try {
			SimpleDateFormat parser = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ITALIAN);
			Date result = parser.parse(jsonDate.replace("T", " "));

			return (result);
		} catch (Exception e) {
			return (null);
		}
	}

	public void componentHide(View component) {
		component.setVisibility(View.INVISIBLE);
	}

	public void componentShow(View component) {
		component.setVisibility(View.VISIBLE);
	}

	private Bitmap resize(Bitmap image, int maxDimSize) {
		if (image.getWidth() <= maxDimSize && image.getHeight() <= maxDimSize)
			return (image);

		int newWidth;
		int newHeight;

		// calcolo delle nuove dimensioni
		if (image.getWidth() > image.getHeight()) {
			double ratio = (((double) image.getHeight()) / ((double) image.getWidth()));
			newWidth = maxDimSize;
			newHeight = (int) (ratio * newWidth);
		} else {
			double ratio = (((double) image.getWidth()) / ((double) image.getHeight()));
			newHeight = maxDimSize;
			newWidth = (int) (ratio * newHeight);
		}

		// ridimensionamento dell'immagine
		Bitmap result = Bitmap.createScaledBitmap(image, newWidth, newHeight, true);

		return (result);
	}

	public String dateFormat(Date date) {
		DateFormat dateFormat = android.text.format.DateFormat.getDateFormat(getApplicationContext());
		String result = dateFormat.format(date);

		return (result);
	}

	protected void winFragmentOpen(Class<?> fragment, HashMap<String, Object> params) {
		try {
			// istanzio il fragment
			MasterFragment fragmentInstance = (MasterFragment) fragment.newInstance();

			// parametri da passare al fragment
			if (params != null && params.size() > 0) {
				Bundle bundle = new Bundle();
				bundle.putSerializable("params", params);
				fragmentInstance.setArguments(bundle);
			}

			// espongo il fragment
			fragmentInstance.show(getFragmentManager(), fragmentInstance.getClass().getSimpleName());
		} catch (Exception e) {
		}
	}

	protected void winFragmentOpen(Class<?> fragment) {
		winFragmentOpen(fragment, null);
	}

	protected void fragmentClose(Fragment fragment) {
		getFragmentManager().beginTransaction().remove(fragment).commit();
	}

	private class DateSetListener implements DatePickerDialog.OnDateSetListener {
		private EditText editText;

		public DateSetListener(EditText editText) {
			this.editText = editText;
		}

		public void onDateSet(DatePicker datePicker, int year, int month, int day) {
			// trasformo i tre valori interi in una data vera e propria
			Calendar calendar = Calendar.getInstance();
			calendar.set(year, month, day);
			Date date = calendar.getTime();

			// stampo la data nel formato richiesto dal sistema (e quindi
			// preferito dall'utente)
			DateFormat dateFormat = android.text.format.DateFormat.getDateFormat(getApplicationContext());
			editText.setText(dateFormat.format(date));
		}
	}

	private class TimeSetListener implements TimePickerDialog.OnTimeSetListener {
		private EditText editText;

		public TimeSetListener(EditText editText) {
			this.editText = editText;
		}

		public void onTimeSet(TimePicker timePicker, int hours, int minutes) {
			editText.setText((new TimeSpan(hours, minutes)).toString());
		}
	}

	public boolean isMyServiceRunning(Class<?> serviceClass) {
		ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
		for (RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
			if (serviceClass.getName().equals(service.service.getClassName())) {
				return true;
			}
		}
		return false;
	}

	public void activityOpenForResult(Class<?> activity, HashMap<String, Object> params, int requestCode) {
		// creo l'intent
		Intent intent = new Intent(this, activity);

		// passaggio dei parametri
		if (params != null && params.size() > 0)
			intent.putExtra("params", params);

		// lancio l'activity
		MasterActivity.this.startActivityForResult(intent, requestCode);
	}

	public void sendStartNotification(String placeName) {
		Intent notificationIntent = new Intent(getApplicationContext(), MainActivity.class);
		notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
		PendingIntent intent = PendingIntent.getActivity(getApplicationContext(), 0, notificationIntent, 0);

		final Notification.Builder builder = new Notification.Builder(this);
		builder.setStyle(
				new Notification.BigTextStyle(builder)
						.bigText(getApplicationContext().getString(R.string.notification_start_note1) + placeName + getApplicationContext().getString(R.string.notification_start_note2))
						.setBigContentTitle(getApplicationContext().getString(R.string.notification_start_message))
						.setSummaryText(getApplicationContext().getString(R.string.notification_summary)))
				.setContentTitle(getApplicationContext().getString(R.string.notification_start_message))
				.setContentText(getApplicationContext().getString(R.string.notification_start_note1) + placeName + getApplicationContext().getString(R.string.notification_start_note2))
				.setSmallIcon(R.drawable.ic_notification).setDefaults(Notification.DEFAULT_SOUND | Notification.DEFAULT_VIBRATE | Notification.DEFAULT_LIGHTS).setAutoCancel(true)
				.setContentIntent(intent);

		NotificationManager notificationManager = (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
		notificationManager.notify(0, builder.build());
	}

	public void sendFinishNotification() {
		Intent notificationIntent = new Intent(getApplicationContext(), MainActivity.class);
		notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
		PendingIntent intent = PendingIntent.getActivity(getApplicationContext(), 0, notificationIntent, 0);

		final Notification.Builder builder = new Notification.Builder(this);
		builder.setStyle(
				new Notification.BigTextStyle(builder).bigText(getApplicationContext().getString(R.string.notification_finish_note))
						.setBigContentTitle(getApplicationContext().getString(R.string.notification_finish_message))
						.setSummaryText(getApplicationContext().getString(R.string.notification_summary)))
				.setContentTitle(getApplicationContext().getString(R.string.notification_finish_message)).setContentText(getApplicationContext().getString(R.string.notification_finish_note))
				.setSmallIcon(R.drawable.ic_notification).setDefaults(Notification.DEFAULT_SOUND | Notification.DEFAULT_VIBRATE | Notification.DEFAULT_LIGHTS).setAutoCancel(true)
				.setContentIntent(intent);

		NotificationManager notificationManager = (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
		notificationManager.notify(0, builder.build());
	}

	public boolean checkCurrentDate(Date date) {
		// Prendo i valori della data inserita
		Calendar calendarIns = Calendar.getInstance();
		calendarIns.setTime(date);
		int insYear = calendarIns.get(Calendar.YEAR);
		int insMonth = calendarIns.get(Calendar.MONTH);
		int insDay = calendarIns.get(Calendar.DAY_OF_MONTH);

		// Prendo i valori della data odierna
		Calendar calendarCurr = Calendar.getInstance();
		calendarCurr.setTime(new Date());
		int CurrYear = calendarCurr.get(Calendar.YEAR);
		int CurrMonth = calendarCurr.get(Calendar.MONTH);
		int CurrDay = calendarCurr.get(Calendar.DAY_OF_MONTH);

		if (insYear < CurrYear) {
			return false;
		} else {
			if (insYear == CurrYear && insMonth < CurrMonth) {
				return false;
			} else {
				if (insYear == CurrYear && insMonth == CurrMonth && insDay < CurrDay) {
					return false;
				}
			}
		}
		return true;
	}
}